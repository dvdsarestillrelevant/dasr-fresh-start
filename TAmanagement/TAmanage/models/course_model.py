# Justin Franecki

from django.db import models
from .ta_model import TA
from .instructor_model import Instructor
from .course_time_model import CourseTime
from .lab_model import Lab


class Course(models.Model):
    course_name = models.CharField(max_length=20, primary_key=True)
    ta = models.ManyToManyField(TA)
    instructor = models.ManyToManyField(Instructor)

    lecture_time = models.OneToOneField(CourseTime, on_delete=models.CASCADE)
    labs = models.ManyToManyField(Lab)

    def get_course_name(self):
        return self.course_name

    def get_lecture_time(self):
        return self.lecture_time

    def get_instructor(self, index):
        return self.instructor.all()[index]

    def get_ta(self, index):
        return self.ta.all()[index]

    def set_ta(self, ta):
        self.ta.add(ta)

    def set_lecture_time(self, time):
        self.lecture_time = time

    def set_course_name(self, name):
        self.course_name = name

    def set_instructor(self, teacher):
        self.instructor.add(teacher)

    def add_lab(self, lab):
        self.labs.add(lab)

    def __str__(self):
        return self.course_name
