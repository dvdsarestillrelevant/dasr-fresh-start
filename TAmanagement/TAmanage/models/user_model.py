# John Egan
from django.db import models
import random
import string


class UserModel(models.Model):
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20)
    email = models.CharField(max_length=20, primary_key=True)
    password = models.CharField(max_length=20)

    def change_password(self, new_password, password2):
        self.password = new_password
        self.save()

    @staticmethod
    def generate_password():
        # generates and returns a password
        password = ''.join(random.choices(string.ascii_letters + string.digits, k=10))
        return password

    def get_name(self):
        return self.first_name

    def get_email(self):
        return self.email

    def get_password(self):
        return self.password

    def set_name(self, inp):
        self.name = inp
        pass

    def set_email(self, inp):
        self.email = inp
        pass

    def set_new_password(self, in_str):
        self.password = in_str
        pass

    @staticmethod
    def display_course():
        from .course_model import Course
        return Course

    @staticmethod
    def list_tas():
        from .ta_model import TA
        return TA

    @staticmethod
    def list_instructors():
        from .instructor_model import Instructor
        return Instructor

    def __str__(self):
        return "{0} {1} ({2})".format(self.first_name,self.last_name,self.email)
    class Meta:
        abstract = True
