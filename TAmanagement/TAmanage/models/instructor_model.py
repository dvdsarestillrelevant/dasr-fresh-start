from .user_model import UserModel
from django.db import models


class Instructor(UserModel):
    phone = models.CharField(max_length=20)
    address = models.CharField(max_length=20)

    def change_instructor_bio(self, phone_number, address):
        self.phone = phone_number
        self.address = address
        self.save()
