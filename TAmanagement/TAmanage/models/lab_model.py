from django.db import models
from .ta_model import TA
from .course_time_model import CourseTime


class Lab(models.Model):
    lab_number = models.IntegerField()
    ta = models.OneToOneField(TA, on_delete=models.CASCADE, null=True)
    time = models.OneToOneField(CourseTime, on_delete=models.CASCADE, null=True)

    def get_lab_number(self):
        return self.lab_number

    def get_time(self):
        return self.time

    def get_ta(self, index):
        return self.ta

    def get_course(self):
        return self.course_set.all()[0]

    def set_ta(self, ta):
        self.ta = ta

    def set_time(self, time):
        self.time = time

    def set_lab_number(self, number):
        self.course_name = number

    def __str__(self):
        return "{} lab: {}".format(str(self.get_course()), str(self.lab_number))

