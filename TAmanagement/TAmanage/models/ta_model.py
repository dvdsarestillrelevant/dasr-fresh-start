from django.db import models
from .user_model import UserModel


class TA(UserModel):
    phone = models.CharField(max_length=20)
    address = models.CharField(max_length=20)

    # preferences aren't OneToOneField(Course) to avoid circular import error w/ course_model :(
    preference1 = models.CharField(max_length=20)
    preference2 = models.CharField(max_length=20)
    preference3 = models.CharField(max_length=20)

    def change_preferences(self, pref1, pref2, pref3):
        if pref3:
            self.preference3 = pref3.course_name
        else:
            self.preference3 = "None"
        if pref2:
            self.preference2 = pref2.course_name
        else:
            self.preference2 = "None"
        if pref1:
            self.preference1 = pref1.course_name
        else:
            self.preference2 = "None"
        self.save()

    def change_ta_bio(self, phone_number, address):
        self.phone = phone_number
        self.address = address
        self.save()

    def schedule_course(self, course):
        from .schedule_model import Schedule
        schedule = Schedule.objects.get(ta=self)
        schedule.course.add(course)

    def schedule_lab(self, lab):
        from .schedule_model import Schedule
        schedule = Schedule.objects.get(ta=self)
        schedule.lab.add(lab)

    def remove_course(self, course):
        from .schedule_model import Schedule
        schedule = Schedule.objects.get(ta=self)
        schedule.course.remove(course)

    def remove_lab(self, lab):
        from .schedule_model import Schedule
        schedule = Schedule.objects.get(ta=self)
        schedule.lab.remove(lab)

    @staticmethod
    def view_my_info():
        return "my_info"
