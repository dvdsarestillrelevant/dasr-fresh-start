from .course_model import Course
from .ta_model import TA
from .lab_model import Lab
from django.db import models


class Schedule(models.Model):
    ta = models.OneToOneField(TA, primary_key=True, on_delete=models.CASCADE)
    lab = models.ManyToManyField(Lab)
    course = models.ManyToManyField(Course)

    def get_ta(self):
        return self.ta

    def set_ta(self, ta):
        self.ta = ta

    # @staticmethod
    # def schedule_course(course, ta):
    #     course.set_ta()
    #     course.save()
    #     course.save()
