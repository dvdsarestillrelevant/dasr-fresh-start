from django.db import models
from datetime import time
import time


class CourseTime(models.Model):
    days = models.CharField(max_length=10)
    start = models.TimeField()
    end = models.TimeField()

    def get_days(self):
        return self.days

    def get_start(self):
        return self.start

    def get_end(self):
        return self.end

    def does_overlap(self, other):
        collision = False
        for i in range(0, len(self.days)):
            print(str(self.days[i]))
            for j in range(0, len(other.get_days())):
                if self.days[i] == other.get_days()[j]:
                    collision = True

        # We get all of the days for the lecture
        # First we then check to see if any of the days match. (ie. Lab: MF; Lecture: MR)
        if collision:
            print("match!")
            # If they are on the same day then we have to check the time
            # Convert course.get_lecture_time().start() to string if not already to compare
            start_time1 = self.start.strftime("%H:%M:%S")
            start_time1 = time.strptime(start_time1.split(',')[0], '%H:%M:%S')

            start_time2 = other.start.strftime("%H:%M:%S")
            start_time2 = time.strptime(start_time2.split(',')[0], '%H:%M:%S')

            end_time1 = self.end.strftime("%H:%M:%S")
            end_time1 = time.strptime(end_time1.split(',')[0], '%H:%M:%S')

            end_time2 = other.end.strftime("%H:%M:%S")
            end_time2 = time.strptime(end_time2.split(',')[0], '%H:%M:%S')

            if start_time2 < start_time1:
                if end_time2 < start_time1:
                    pass
                else:
                    return True
            elif start_time2 > start_time1:
                if end_time1 < start_time2:
                    pass
                else:
                    return True
            else:
                # They both start at the same time!
                return True

        return False
