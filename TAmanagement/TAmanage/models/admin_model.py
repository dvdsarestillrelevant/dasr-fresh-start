# David Ruiz
from .user_model import UserModel
from .course_model import Course
from .course_time_model import CourseTime
from .lab_model import Lab
from .schedule_model import Schedule


class Admin(UserModel):
    # user based methods
    @staticmethod
    def add_ta(model):
        # creates a ta
        schedule = Schedule(ta=model)
        schedule.save()

    @staticmethod
    def add_instructor(model):
        # creates an instructor
        pass

    @staticmethod
    def add_course(name, days, start, end):

        time = CourseTime(days=days, start=start, end=end)
        time.save()

        course = Course(course_name=name, lecture_time=time)
        course.save()

    @staticmethod
    def add_lab(course, number, days, start, end):
        time = CourseTime(days=days, start=start, end=end)
        time.save()

        lab = Lab(lab_number=number, time=time)
        lab.save()

        course.add_lab(lab)
        course.save()

    @staticmethod
    def assign_ta(ta, course, lab, *args):

        course.set_ta(ta)
        course.save()

        if lab:
            if lab.ta:  # if lab already has a ta that will be replaced, remove that TA from the course
                lab.get_course().ta.remove(lab.ta)

            lab.set_ta(ta)
            lab.save()

        course.save()

    @staticmethod
    def assign_instructor(instructor, course):
        course.instructor.add(instructor)

    def change_user_password(self, user, password):
        # given a user, then change the current password with a given one
        pass

    @staticmethod
    def delete_user(*args):
        # This method will take in a given user and delete its instance.
        for user in args:
            if user is None:
                pass
            else:
                user.delete()

    @staticmethod
    def unassign_ta(ta, course):
        lab = course.labs.filter(ta=ta)
        if len(lab) >= 1:
            lab[0].ta = None
            lab[0].save()

        course.ta.remove(ta)
        course.save()

    @staticmethod
    def unassign_instructor(instructor, course):
        course.instructor.remove(instructor)
        course.save()
