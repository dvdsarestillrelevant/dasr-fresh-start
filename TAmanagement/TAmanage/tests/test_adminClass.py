from django.test import TestCase
from ..models.admin_model import Admin
from ..models.course_time_model import CourseTime
from ..models.ta_model import TA
from ..models.instructor_model import Instructor
from ..models.course_model import Course


def assert_raise(ex, meth):
    try:
        meth
    except ex:
        return True
    else:
        return False


class MyTestCase(TestCase):
    def setUp(self):
        self.timeA = CourseTime(days='MW', start='08:00', end='10:45')
        self.timeA.save()
        self.a1 = Admin(first_name="john", last_name='doe', email="jdoe@uwm.edu")
        self.a1.save()
        self.a2 = Admin(first_name="Jane", last_name='doe')
        self.a2.save()
        self.a3 = Admin(email="jdoe2@uwm.edu")
        self.a3.save()
        self.TA1 = TA(first_name="Alex", last_name='sumthing', email="asum@uwm.edu")
        self.TA1.save()
        self.IN1 = Instructor(first_name="Mandy", last_name='Whatsitwho', email="mwhat@uwm.edu")
        self.IN1.save()
        self.c1 = Course(course_name='CS001', lecture_time=self.timeA)
        self.c1.save()

    def test_admin_form(self):
        # test if admin is properly setup.
        self.assertIsNotNone(self.a1.email, "Email should not be None on creation")
        self.assertIsNotNone(self.a1.first_name, "name cannot be None on creation")
        self.assertIsNotNone(self.a1.last_name, "name cannot be None on creation")

    # def test_get_password(self):
    #     # get back admin password
    #     self.fail()

    def test_get_email(self):
        self.assertEqual(self.a1.email, "jdoe@uwm.edu")
        self.assertEqual(self.a3.email, "jdoe2@uwm.edu")



    # def test_change_user_password(self):
    #     # test that admin is able to change a users password
    #     self.fail()


    #
    # def test_assign_Instructor_course(self):
    #     # test what happens if you assign an instructor to a course
    #     # test if we put a None instructor in instructor param
    #     # test if we don't put a instructor in param
    #     # test if we put a None course in a course param
    #     # test if we give it nothing
    #     self.a1.assign_instructor("mwhat@uwm.edu", 'CS001')
    #     self.assertEqual(Course.objects.get(course_name="CS001").get_instructor(0), Instructor.objects.get(email="mwhat@uwm.edu"), "TA not set in class properly.")
    #     self.assertTrue(assert_raise(Instructor.DoesNotExist, lambda: self.a1.assign_instructor("asum@uwm.edu", 'CS001')), "TAs not an instructor")
    #     self.assertTrue(assert_raise(Course.DoesNotExist, lambda: self.a1.assign_instructor("mwhat@uwm.edu", 'CS002')), "Class does not exist")

