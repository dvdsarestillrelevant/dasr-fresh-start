from django.test import TestCase
from ..models.ta_model import TA


class TestTA(TestCase):
    def setUp(self):
        self.a1 = TA(first_name="Bob", last_name='Poo', email="Poo23@uwm.edu")
        self.a1.save()
        self.a2 = TA(first_name="Ted", last_name='Turner', email="Turner31@uwm.edu")
        self.a2.save()
        self.a3 = TA(first_name="Barry", last_name='Benson', email="Benson72@uwm.edu")
        self.a3.save()

    def test_get_address(self):
        self.a1.address = "3016 N Fredrick"
        self.a1.save()
        self.a2.address = "204 W Resorvoir Ave"
        self.a2.save()
        self.a3.address = "3333 333"
        self.a3.save()
        # Test in python here...
        self.assertEqual("3016 N Fredrick", self.a1.address)
        self.assertEqual("204 W Resorvoir Ave", self.a2.address)
        # Test against the data base here
        self.assertEqual(len(TA.objects.filter(address="3016 N Fredrick")), 1)
        self.assertEqual(len(TA.objects.filter(address="204 W Resorvoir Ave")), 1)
        self.assertEqual(len(TA.objects.filter(address="3333 333")), 1)

    def test_get_phone(self):
        self.a1.phone = "262-490-4808"
        self.a1.save()
        self.a2.phone = "262-443-1683"
        self.a2.save()
        self.a3.phone = "414-223-4444"
        self.a3.save()
        self.assertEqual(len(TA.objects.filter(phone="262-490-4808")), 1)
        self.assertEqual(len(TA.objects.filter(phone="262-443-1683")), 1)
        self.assertEqual(len(TA.objects.filter(phone="414-223-4444")), 1)

