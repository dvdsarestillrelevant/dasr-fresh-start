# John Egan
from django.test import TestCase
from ..models.instructor_model import Instructor as User

#" User" refers to Instructer in instructor_model

# class testUser(TestCase):
#     def setUp(self):
#         self.u1 = User(name="Dude Man", email="dman@uwm.edu")
#         self.u1.save()
#         self.u2 = User(name="Mr Guy", email="mguy@uwm.edu")
#         self.u2.save()
#         pass
#
#     def test_get_name(self):
#         # test get_name works
#         self.assertEqual(self.u1.get_name(), "Dude Man")
#         self.assertEqual(self.u2.get_name(), "Mr Guy")
#         pass
#
#     def test_set_name(self):
#         # test set_name works
#         self.u1.set_name("Dude Person")
#         self.assertEqual(self.u1.get_name(), "Dude Person")
#         # reset name to original
#         self.u1.set_name("Dude Man")
#         self.assertEqual(self.u1.get_name(), "Dude Man")
#
#         pass
#
#     def test_set_name_inputs(self):
#         # Test non-strings
#         self.assertRaises(Exception, self.u2.set_name(3))
#         self.assertRaises(Exception, self.u2.set_name(True))
#         self.assertRaises(Exception, self.u2.set_name(3.45))
#         self.assertRaises(Exception, self.u2.set_name('c'))
#
#         pass
#
#     def test_get_email(self):
#         # test get_email
#         self.assertEqual(self.u1.get_email(), "dman@uwm.edu")
#         self.assertEqual(self.u2.get_email(), "mguy@uwm.edu")
#         pass
#
#     def test_set_email(self):
#         # test set_email works
#         self.u1.set_email("dperson@uwm.edu")
#         self.assertEqual(self.u1.get_email(), "dperson@uwm.edu")
#
#         # change email back to original
#         self.u1.set_email("dman@uwm.edu")
#         self.assertEqual(self.u1.get_email(), "dman@uwm.edu")
#
#         pass
#
#     def test_set_email_inputs(self):
#         # test non-strings
#         self.assertRaises(Exception, self.u2.set_email(3))
#         self.assertRaises(Exception, self.u2.set_email(True))
#         self.assertRaises(Exception, self.u2.set_email(3.45))
#         self.assertRaises(Exception, self.u2.set_email('c'))
#
#         # test invalid strings
#         self.assertRaises(Exception, self.u2.set_email("mguy"))
#         self.assertRaises(Exception, self.u2.set_email("@uwm.edu"))
#         self.assertRaises(Exception, self.u2.set_email("mguy@uwm"))
#         self.assertRaises(Exception, self.u2.set_email("mguy@.edu"))
#         pass
#
#     def test_get_password(self):
#         # test password exists and is a string
#         self.assertIsInstance(self.u1.get_password(), str)
#         pass
#
#     def test_password_generate(self):
#         # test set_new_password creates and sets a new password
#         password1 = self.u1.generate_password()
#         self.assertIsInstance(password1, str)
#         password2 = self.u1.generate_password()
#         self.assertNotEqual(password1, password2)
#         pass
#
#     def test_set_new_password(self):
#         # test generate_password creates a unique password
#         password1 = self.u1.get_password()
#         self.u1.set_new_password()
#         self.assertNotEqual(password1, self.u1.get_password())
#         pass
