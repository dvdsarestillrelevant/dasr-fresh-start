# Andrew's personal hell
from django.test import TestCase
from TAmanage.driver import Driver
# assumed import statements
from ..models.ta_model import TA
from ..models.instructor_model import Instructor
from ..models.course_model import Course
from ..models.admin_model import Admin


class DriverLoginLogoutTestCase(TestCase):
    def setUp(self):
        Driver.command("logout")

    ####################
    # Behavior of login command:
    # Prompts for username
    # Prompts for password
    # changes user value of driver to user if found
    # else: re-prompts for username etc.
    # exceptions: none
    # user-story: Admins should be able to login first
    def test_login_one(self):
        # initial command
        out = Driver.command("login")
        self.assertEqual("Enter a Username.", out, "initial out improper string")
        self.assertEqual("Login", Driver.args[0], "initial driver args faulty value")
        self.assertEqual(1, len(Driver.args), "initial driver args wrong size")

        # second argument
        out = Driver.command("admin")
        self.assertEqual("Enter the Password for {0}.".format("admin"), out, "proper second out improper string")
        self.assertEqual("Login", Driver.args[0], "proper second driver args faulty value index 0")
        self.assertEqual("admin", Driver.args[1], "proper second driver args faulty value index 1")
        self.assertEqual(2, len(Driver.args), "proper second driver args wrong size")
        # third argument
        out = Driver.command("password")
        self.assertEqual("Log in as {0} success!".format("admin"), out, "final out improper string")
    def test_login_two(self):
        bad = "nonexistant"
        # initial command
        out = Driver.command("login")
        self.assertEqual("Enter a Username.", out, "initial out improper string")
        self.assertEqual("Login", Driver.args[0], "initial driver args faulty value")
        self.assertEqual(1, len(Driver.args), "initial driver args wrong size")

        # second argument
        out = Driver.command(bad)
        self.assertEqual("Enter the Password for {0}.".format(bad), out, "second out improper string")
        self.assertEqual("Login", Driver.args[0], "second driver args faulty value index 0")
        self.assertEqual(bad, Driver.args[1], "second driver args faulty value index 1")
        self.assertEqual(2, len(Driver.args), "second driver args wrong size")
        # third argument
        out = Driver.command("nonexistant")
        self.assertEqual("Incorrect username password combination. Enter a Username.", out, "second out improper string")
        self.assertEqual("Login", Driver.args[0], "third driver args faulty value index 0")
        self.assertEqual(1, len(Driver.args), "third driver args wrong size")

        # second argument
        out = Driver.command("admin")
        self.assertEqual("Enter the Password for {0}.".format("admin"), out, "proper second out improper string")
        self.assertEqual("Login", Driver.args[0], "proper second driver args faulty value index 0")
        self.assertEqual("admin", Driver.args[1], "proper second driver args faulty value index 1")
        self.assertEqual(2, len(Driver.args), "proper second driver args wrong size")
        # third argument
        out = Driver.command("password")
        self.assertEqual("Log in as {0} success!".format("admin"), out, "final out improper string")
        self.assertEqual(0, len(Driver.args), "final driver args wrong size")
        # make sure login actually worked
        self.assertIsInstance(Driver.user, Admin, "user not logged in right.")

    def test_login_fail_loop(self):
        bad = "nonexistant"
        # initial command
        out = Driver.command("login")
        self.assertEqual("Enter a Username.", out, "initial out improper string")
        self.assertEqual("Login", Driver.args[0], "initial driver args faulty value")
        self.assertEqual(1, len(Driver.args), "initial driver args wrong size")

        # repeat fails several times then finally pass
        for i in range(20):
            # second argument
            out = Driver.command(bad)
            self.assertEqual("Enter the Password for {0}.".format(bad), out, "second out improper string")
            self.assertEqual("Login", Driver.args[0], "second driver args faulty value index 0")
            self.assertEqual(bad, Driver.args[1], "second driver args faulty value index 1")
            self.assertEqual(2, len(Driver.args), "second driver args wrong size")
            # third argument
            out = Driver.command(bad)
            self.assertEqual("Incorrect username password combination. Enter a Username.", out,
                             "second out improper string")
            self.assertEqual("Login", Driver.args[0], "third driver args faulty value index 0")
            self.assertEqual(1, len(Driver.args), "third driver args wrong size")

        # second argument
        out = Driver.command("admin")
        self.assertEqual("Enter the Password for {0}.".format("admin"), out, "proper second out improper string")
        self.assertEqual("Login", Driver.args[0], "proper second driver args faulty value index 0")
        self.assertEqual("admin", Driver.args[1], "proper second driver args faulty value index 1")
        self.assertEqual(2, len(Driver.args), "proper second driver args wrong size")
        # third argument
        out = Driver.command("password")
        self.assertEqual("Log in as {0} success!".format("admin"), out, "final out improper string")
        self.assertEqual(0, len(Driver.args), "final driver args wrong size")
        # make sure login actually worked
        self.assertIsInstance(Driver.user, Admin, "user not logged in right.")

    def test_repeated_login(self):
        #initial
        # initial command
        out = Driver.command("login")
        self.assertEqual("Enter a Username.", out, "initial out improper string")
        self.assertEqual("Login", Driver.args[0], "initial driver args faulty value")
        self.assertEqual(1, len(Driver.args), "initial driver args wrong size")
        # second argument
        out = Driver.command("admin")
        self.assertEqual("Enter the Password for {0}.".format("admin"), out, "proper second out improper string")
        self.assertEqual("Login", Driver.args[0], "proper second driver args faulty value index 0")
        self.assertEqual("admin", Driver.args[1], "proper second driver args faulty value index 1")
        self.assertEqual(2, len(Driver.args), "proper second driver args wrong size")
        # third argument
        out = Driver.command("password")
        self.assertEqual("Log in as {0} success!".format("admin"), out, "final out improper string")
        self.assertEqual(0, len(Driver.args), "final driver args wrong size")
        # make sure login actually worked
        self.assertIsInstance(Driver.user, Admin, "user not logged in right.")
        #second login
        out = Driver.command("login")
        self.assertEqual("User already logged in.", out, "initial out improper string")
        self.assertEqual(0, len(Driver.args), "resulting driver args wrong size")

    ####################
    # Behavior of logout command:
    # logs current user out, otherwise returns list of commands
    # exceptions: none
    # user-story: All users should be able to log out.
    def test_logout(self):
        out = Driver.command("logout")
        self.assertEqual("No one logged in.", out, "initial out improper string")
        self.assertEqual(0, len(Driver.args), "resulting driver args wrong size")
        Driver.command("login")
        Driver.command("admin")
        Driver.command("password")
        out = Driver.command("logout")
        self.assertEqual("Logout successful.", out, "initial out improper string")
        self.assertEqual(0, len(Driver.args), "resulting driver args wrong size")
        out = Driver.command("logout")
        self.assertEqual("No one logged in.", out, "initial out improper string")
        self.assertEqual(0, len(Driver.args), "resulting driver args wrong size")


# Overall this Test case addresses the user story:
#       "All users should be able to enter commands to interact with the program"
class DriverTestCase(TestCase):
    def setUp(self):
        ############
        # should create 3 TAs, 3 instructors, and 3 courses, but is kinda sketch without you guys' stuff
        ############
        if len(TA.objects.all()) == 0:
            t1 = TA(name="Bob Poo", email="Poo23@uwm.edu")
            t1.save()
            t2 = TA(name="Ted Turner", email="Turner31@uwm.edu")
            t2.save()
            t3 = TA(name="Barry Benson", email="Benson72@uwm.edu")
            t3.save()
            i1 = Instructor(name="Jimmy Dean", email="Dean@uwm.edu")
            i1.save()
            i2 = Instructor(name="Harold Miller", email="Miller7@uwm.edu")
            i2.save()
            i3 = Instructor(name="Gerald Furner", email="Gfurner@uwm.edu")
            i3.save()
            c1 = Course(name="Math 1")
            c1.save()
            c2 = Course(name="English 2")
            c2.save()
            c3 = Course(name="Science 4")
            c3.save()

        # log in as admin so the commands work.
        Driver.command("login")
        Driver.command("admin")
        Driver.command("password")

    ####################
    # Behavior of add ta command:
    # Prompts for name
    # Prompts for email
    # creates new instance of ta model in the database
    # exceptions: none
    # user-story: All users should be separated into Admin, TA, and Teachers.
    def test_add_ta(self):
        # initial command
        out = Driver.command("add ta")
        self.assertEqual("Enter the TA's name.", out, "initial out improper string")
        self.assertEqual("Add TA", Driver.args[0], "initial driver args faulty value")
        self.assertEqual(1, len(Driver.args), "initial driver args wrong size")

        # second argument
        out = Driver.command("John Doe")
        self.assertEqual("Enter the email for John Doe.", out, "second out improper string")
        self.assertEqual("Add TA", Driver.args[0], "secondary driver args faulty value index 0")
        self.assertEqual("John Doe", Driver.args[1], "secondary driver args faulty value index 1")
        self.assertEqual(2, len(Driver.args), "second driver args wrong size")
        # third argument
        out = Driver.command("Doe123@uwm.edu")
        self.assertEqual("TA created John Doe at Doe123@uwm.edu.", out,
                         "second out improper string")
        self.assertEqual(0, len(Driver.args), "third driver args wrong size")

        ####################
        # this next line is sketchy without you guys' stuff
        self.assertEqual(len(TA.objects.filter(name="John Doe")), 1, "no new ta object")

    ####################
    # Behavior of add course command:
    # Prompts for name-thingy
    # creates new instance of course model in the database
    # exceptions: none
    # user-story: Admins can create courses
    def test_add_course(self):
        # initial command
        out = Driver.command("add course")
        self.assertEqual("Enter the course's name.", out, "initial out improper string")
        self.assertEqual("Add Course", Driver.args[0], "initial driver args faulty value")
        self.assertEqual(1, len(Driver.args), "initial driver args wrong size")

        # second argument
        out = Driver.command("Math 2")
        self.assertEqual("Course created named Math 2.", out,
                         "second out improper string")
        self.assertEqual(0, len(Driver.args), "second driver args wrong size")

        ####################
        # this next line is sketchy without you guys' stuff
        self.assertEqual(len(Course.objects.filter(name="Math 2")), 1, "no new course object")

    ####################
    # Behavior of add instructor command:
    # Prompts for name
    # Prompts for email
    # creates new instance of instructor model in the database
    # exceptions: none
    # user-story: All users should be separated into Admin, TA, and Teachers.
    def test_add_instructor(self):
        # initial command
        out = Driver.command("add instructor")
        self.assertEqual("Enter the instructor's name.", out, "initial out improper string")
        self.assertEqual("Add Instructor", Driver.args[0], "initial driver args faulty value")
        self.assertEqual(1, len(Driver.args), "initial driver args wrong size")

        # second argument
        out = Driver.command("John Doe")
        self.assertEqual("Enter the email for John Doe.", out, "second out improper string")
        self.assertEqual("Add Instructor", Driver.args[0], "secondary driver args faulty value index 0")
        self.assertEqual("John Doe", Driver.args[1], "secondary driver args faulty value index 1")
        self.assertEqual(2, len(Driver.args), "second driver args wrong size")
        # third argument
        out = Driver.command("Doe123@uwm.edu")
        self.assertEqual("Instructor created John Doe at Doe123@uwm.edu.", out,
                         "second out improper string")
        self.assertEqual(0, len(Driver.args), "third driver args wrong size")

        ####################
        # this next line is sketchy without you guys' stuff
        self.assertEqual(len(Instructor.objects.filter(name="John Doe")), 1, "no new instructor object")

    ####################
    # Behavior of assign ta command:
    # Prompts for ta name
    # Prompts for course name-thingy
    # finds ta and finds course, if both found: adds the ta to the list of tas for the course
    # else: returns failure message
    # exceptions: none
    # user-story: Admins should be able to set up courses
    def test_assign_ta(self):
        # initial command
        out = Driver.command("assign ta")
        self.assertEqual("Enter the TA's name.", out, "initial out improper string")
        self.assertEqual("Assign TA", Driver.args[0], "initial driver args faulty value")
        self.assertEqual(1, len(Driver.args), "initial driver args wrong size")

        # second argument
        out = Driver.command("Bob Poo")
        self.assertEqual("Enter the course for Bob Poo.", out, "second out improper string")
        self.assertEqual("Assign TA", Driver.args[0], "secondary driver args faulty value index 0")
        self.assertEqual("Bob Poo", Driver.args[1], "secondary driver args faulty value index 1")
        self.assertEqual(2, len(Driver.args), "second driver args wrong size")
        # third argument
        out = Driver.command("Math 1")
        self.assertEqual("TA Bob Poo assigned to Math 1.", out,
                         "second out improper string")
        self.assertEqual(0, len(Driver.args), "third driver args wrong size")

        # also check with name that don't exist
        out = Driver.command("assign ta")
        self.assertEqual("Enter the TA's name.", out, "initial out improper string")
        self.assertEqual("Assign TA", Driver.args[0], "initial driver args faulty value")
        self.assertEqual(1, len(Driver.args), "initial driver args wrong size")

        # second argument
        out = Driver.command("Missing Name")
        error_msg = "Command Failure: TA, \"Missing Name\", not found."
        self.assertEqual(error_msg, out, "second out improper string")
        self.assertEqual(0, len(Driver.args), "bad ta name driver args wrong size")

        # also check with course that don't exist
        out = Driver.command("assign ta")
        self.assertEqual("Enter the TA's name.", out, "initial out improper string")
        self.assertEqual("Assign TA", Driver.args[0], "initial driver args faulty value")
        self.assertEqual(1, len(Driver.args), "initial driver args wrong size")

        # second argument
        out = Driver.command("Bob Poo")
        self.assertEqual("Enter the course for Bob Poo.", out, "second out improper string")
        self.assertEqual("Assign TA", Driver.args[0], "secondary driver args faulty value index 0")
        self.assertEqual("Bob Poo", Driver.args[1], "secondary driver args faulty value index 1")
        self.assertEqual(2, len(Driver.args), "second driver args wrong size")
        # third argument
        out = Driver.command("Missing Name")
        error_msg = "Command Failure: course, \"Missing Name\", not found."
        self.assertEqual(error_msg, out, "second out improper string")
        self.assertEqual(0, len(Driver.args), "bad course name driver args wrong size")

        # check db
        self.assertEqual(Course.objects.filter(name="Math 1")[0].get_ta(0), TA.objects.filter(name="Bob Poo")[0],
                         "TA not set in class properly.")
        self.assertEqual(len(Course.objects.filter(name="Math 1")[0].ta.all()), 1, "TA count for course wrong.")


    ####################
    # Behavior of assign instructor command:
    # Prompts for assign instructor name
    # Prompts for course name-thingy
    # finds ta and finds course, if both found: adds the ta to the list of tas for the course
    # else: returns failure message
    # exceptions: none
    # user-story: Admins should be able to set up courses
    def test_assign_instructor(self):
        # initial command
        out = Driver.command("assign Instructor")
        self.assertEqual("Enter the instructor's name.", out, "initial out improper string")
        self.assertEqual("Assign Instructor", Driver.args[0], "initial driver args faulty value")
        self.assertEqual(1, len(Driver.args), "initial driver args wrong size")

        # second argument
        out = Driver.command("Jimmy Dean")
        self.assertEqual("Enter the course for Jimmy Dean.", out, "second out improper string")
        self.assertEqual("Assign Instructor", Driver.args[0], "secondary driver args faulty value index 0")
        self.assertEqual("Jimmy Dean", Driver.args[1], "secondary driver args faulty value index 1")
        self.assertEqual(2, len(Driver.args), "second driver args wrong size")
        # third argument
        out = Driver.command("Math 1")
        self.assertEqual("Instructor Jimmy Dean assigned to Math 1.", out,
                         "second out improper string")
        self.assertEqual(0, len(Driver.args), "third driver args wrong size")

        # also check with name that don't exist
        out = Driver.command("assign instructor")
        self.assertEqual("Enter the instructor's name.", out, "initial out improper string")
        self.assertEqual("Assign Instructor", Driver.args[0], "initial driver args faulty value")
        self.assertEqual(1, len(Driver.args), "initial driver args wrong size")

        # second argument
        out = Driver.command("Missing Name")
        error_msg = "Command Failure: Instructor, \"Missing Name\", not found."
        self.assertEqual(error_msg, out, "second out improper string")
        self.assertEqual(0, len(Driver.args), "bad ta name driver args wrong size")

        # also check with course that don't exist
        out = Driver.command("assign instructor")
        self.assertEqual("Enter the instructor's name.", out, "initial out improper string")
        self.assertEqual("Assign Instructor", Driver.args[0], "initial driver args faulty value")
        self.assertEqual(1, len(Driver.args), "initial driver args wrong size")

        # second argument
        out = Driver.command("Jimmy Dean")
        self.assertEqual("Enter the course for Jimmy Dean.", out, "second out improper string")
        self.assertEqual("Assign Instructor", Driver.args[0], "secondary driver args faulty value index 0")
        self.assertEqual("Jimmy Dean", Driver.args[1], "secondary driver args faulty value index 1")
        self.assertEqual(2, len(Driver.args), "second driver args wrong size")
        # third argument
        out = Driver.command("Missing Name")
        error_msg = "Command Failure: course, \"Missing Name\", not found."
        self.assertEqual(error_msg, out, "second out improper string")
        self.assertEqual(0, len(Driver.args), "bad course name driver args wrong size")

        # check db
        self.assertEqual(Course.objects.filter(name="Math 1")[0].get_instructor(0),
                         Instructor.objects.filter(name="Jimmy Dean")[0], "TA not set in class properly.")
        self.assertEqual(len(Course.objects.filter(name="Math 1")[0].instructor.all()), 1,
                         "Instructor count for course wrong.")

    ####################
    # Behavior of list tas command:
    # returns string listing all tas
    # exceptions: none
    # user-story: Admins want a collection of TA's and the courses they are TA-ing and want to TA
    def test_list_tas(self):
        out = Driver.command("list tas")
        self.assertEqual("All TAs: Bob Poo, Ted Turner, Barry Benson", out, "initial out improper string")
        self.assertEqual(0, len(Driver.args), "resulting driver args wrong size")

    ####################
    # Behavior of list course command:
    # returns string listing all courses
    # exceptions: none
    # user-story: Admins want a collection of TA's and the courses they are TA-ing and want to TA
    def test_list_courses(self):
        out = Driver.command("list courses")
        self.assertEqual(Course, out, "improper return.")
        self.assertEqual(0, len(Driver.args), "resulting driver args wrong size")

    ####################
    # Behavior of list instructors command:
    # returns string listing all instructors
    # exceptions: none
    # user-story:
    def test_list_instructors(self):
        out = Driver.command("list instructors")
        self.assertEqual("All Instructors: Jimmy Dean, Harold Miller, Gerald Furner", out, "initial out improper string")
        self.assertEqual(0, len(Driver.args), "resulting driver args wrong size")

    ####################
    # if a non-existent command is attempted such as "Abraham lincoln" or 42:
    # list of commands is returned
    # user-story: Unspecific/serves overall user story.
    def test_bad_commands(self):
        out = Driver.command("Abraham Lincoln")
        self.assertEqual("Enter a command from below.", out, "initial out improper string")
        self.assertEqual(0, len(Driver.args), "resulting driver args wrong size")
        out = Driver.command(42)
        self.assertEqual("Enter a command from below.", out, "initial out improper string")
        self.assertEqual(0, len(Driver.args), "resulting driver args wrong size")
        out = Driver.command(True)
        self.assertEqual("Enter a command from below.", out, "initial out improper string")
        self.assertEqual(0, len(Driver.args), "resulting driver args wrong size")
        out = Driver.command(32.5)
        self.assertEqual("Enter a command from below.", out, "initial out improper string")
        self.assertEqual(0, len(Driver.args), "resulting driver args wrong size")
        out = Driver.command(["array", "of", "crap"])
        self.assertEqual("Enter a command from below.", out, "initial out improper string")
        self.assertEqual(0, len(Driver.args), "resulting driver args wrong size")

############
# How tests could be improved (Andrew's admission to laziness):
# add two more tests to check login flow:
# A) initial login is proper
# B) extensive failure loop (maybe use for loop to fail 20 times or something)
# Overall, my tests feel gross because I'm assuming a lot,
#     like how command() will always get string input for arguments.
# Stupid for now I guess, but something that can change later.
# Also, I do not test non-admin accounts, but that's another sprint...
