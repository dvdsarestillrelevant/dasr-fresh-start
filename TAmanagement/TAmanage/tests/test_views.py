# Justin Franecki

"""
TODO
    Test for Lab on Assign(AssignTA and AssignInstructor) and Add(AddLab)
        On Hold til course_time is done
"""
from datetime import time

from django.test import TestCase, Client
from ..models.ta_model import TA
from ..models.instructor_model import Instructor
from ..models.user_model import UserModel
from ..models.course_model import Course
from ..models.admin_model import Admin
from ..models.lab_model import Lab
from ..models.course_time_model import CourseTime


class TestHome_notloggedin(TestCase):
    def setUp(self):
        self.c = Client()
        self.c.post('/', {'command': 'logout'})

    def test_home_GET(self):
        response = self.c.get('/')
        self.assertEqual(response.status_code, 200)

    def test_home_notloggedin_empty(self):
        response = self.c.post('/', {'command': ''})
        self.assertEqual(response.status_code, 200)

    def test_home_notloggedin_login(self):
        response = self.c.post('/', {'command': 'login'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'formview.html', 'Expected formview.html')

    def test_home_notloggedin_addTa(self):
        response = self.c.post('/', {'command': 'add ta'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'formview.html', 'Expected formview.html')

    def test_home_notloggedin_addCourse(self):
        response = self.c.post('/', {'command': 'add course'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'formview.html', 'Expected formview.html')

    def test_home_notloggedin_addInstructor(self):
        response = self.c.post('/', {'command': 'add instructor'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'formview.html', 'Expected formview.html')

    def test_home_notloggedin_assignTA(self):
        response = self.c.post('/', {'command': 'assign ta'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'formview.html', 'Expected formview.html')

    def test_home_notloggedin_assignInstructor(self):
        response = self.c.post('/', {'command': 'assign instructor'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'formview.html', 'Expected formview.html')

    def test_home_notloggedin_changePassword(self):
        response = self.c.post('/', {'command': 'change password'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'formview.html', 'Expected formview.html')

    def test_home_notloggedin_listTAs(self):
        response = self.c.post('/', {'command': 'list tas'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'formview.html', 'Expected formview.html')

    def test_home_notloggedin_listInstructors(self):
        response = self.c.post('/', {'command': 'list tas'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'formview.html', 'Expected formview.html')

    def test_home_notloggedin_listCourses(self):
        response = self.c.post('/', {'command': 'list courses'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'formview.html', 'Expected formview.html')

    def test_home_notloggedin_ChangePassword(self):
        response = self.c.post('/', {'command': 'change password'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'formview.html', 'Expected formview.html')

    def test_home_notloggedin_AddLab(self):
        response = self.c.post('/', {'command': 'add lab'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'formview.html', 'Expected formview.html')

    def test_home_notloggedin_DeleteUser(self):
        response = self.c.post('/', {'command': ''})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'formview.html', 'Expected formview.html')

    def test_home_notloggedin_(self):
        response = self.c.post('/', {'command': 'delete user'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'formview.html', 'Expected formview.html')

    def test_home_notloggedin_AppendSchedule(self):
        response = self.c.post('/', {'command': 'append schedule'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'formview.html', 'Expected formview.html')

    def test_home_notloggedin_ChangeTaBio(self):
        response = self.c.post('/', {'command': 'change ta bio'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'formview.html', 'Expected formview.html')

    def test_home_notloggedin_ChangePreferences(self):
        response = self.c.post('/', {'command': 'change preferences'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'formview.html', 'Expected formview.html')

    def test_home_notloggedin_ChangeInstructorBio(self):
        response = self.c.post('/', {'command': 'change instructor bio'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'formview.html', 'Expected formview.html')

    def test_home_notloggedin_EditSchedule(self):
        response = self.c.post('/', {'command': 'edit schedule'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'formview.html', 'Expected formview.html')

    def test_home_notloggedin_RemoveCourse(self):
        response = self.c.post('/', {'command': 'remove course'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'formview.html', 'Expected formview.html')

    def test_home_notloggedin_RemoveLab(self):
        response = self.c.post('/', {'command': 'remove lab'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'formview.html', 'Expected formview.html')

    def test_home_notloggedin_UnassignTa(self):
        response = self.c.post('/', {'command': 'unassign ta'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'formview.html', 'Expected formview.html')

    def test_home_notloggedin_UnassignInstructor(self):
        response = self.c.post('/', {'command': 'unassign instructor'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'formview.html', 'Expected formview.html')

    def test_home_notloggedin_ScheduleCourse(self):
        response = self.c.post('/', {'command': 'schedule course'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'formview.html', 'Expected formview.html')

    def test_home_notloggedin_ScheduleLab(self):
        response = self.c.post('/', {'command': 'schedule lab'})
        self.assertEqual(response.status_code, 200)

    def test_home_notloggedin_ViewMyInfo(self):
        response = self.c.post('/', {'command': 'view my info'})
        self.assertEqual(response.status_code, 200)

    def test_home_notloggedin_logout(self):
        response = self.c.post('/', {'command': 'logout'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'formview.html', 'Expected formview.html')


class TestHome_admin(TestCase):
    def setUp(self):
        self.c = Client()
        admin = Admin(email='admin', password='password')
        admin.save()
        self.c.post('/login', {'email': 'admin', 'password': 'password'})

    def test_home_admin_empty(self):
        response = self.c.post('/', {'command': ''})
        self.assertEqual(response.status_code, 200)

    def test_home_admin_login(self):
        response = self.c.post('/', {'command': 'login'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'index.html', 'Expected index.html')
        self.assertEqual(response.context['message'], 'Invalid Command.')

    def test_home_admin_addTA(self):
        response = self.c.post('/', {'command': 'add ta'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'formview.html', 'Expected formview.html')

    def test_home_admin_addCourse(self):
        response = self.c.post('/', {'command': 'add course'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'formview.html', 'Expected formview.html')

    def test_home_admin_addInstructor(self):
        response = self.c.post('/', {'command': 'add instructor'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'formview.html', 'Expected formview.html')

    def test_home_admin_addLab(self):
        response = self.c.post('/', {'command': 'add lab'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'formview.html', 'Expected formview.html')

    def test_home_admin_assignTA(self):
        response = self.c.post('/', {'command': 'assign ta'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'formview.html', 'Expected formview.html')

    def test_home_admin_assignInstructor(self):
        response = self.c.post('/', {'command': 'assign instructor'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'formview.html', 'Expected formview.html')

    def test_home_admin_changePassword(self):
        response = self.c.post('/', {'command': 'change password'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'formview.html', 'Expected formview.html')

    def test_home_admin_listInstructors(self):
        response = self.c.post('/', {'command': 'list tas'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'displayTA.html', 'Expected displayTA.html')

    def test_home_admin_listCourses(self):
        response = self.c.post('/', {'command': 'list courses'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'displaycourse.html', 'Expected displaycourse.html')

    def test_home_admin_ChangePassword(self):
        response = self.c.post('/', {'command': 'change password'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'formview.html', 'Expected formview.html')

    def test_home_admin_AddLab(self):
        response = self.c.post('/', {'command': 'add lab'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'formview.html', 'Expected formview.html')

    def test_home_admin_DeleteUser(self):
        response = self.c.post('/', {'command': 'delete user'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'formview.html', 'Expected formview.html')

    def test_home_admin_AppendSchedule(self):
        response = self.c.post('/', {'command': 'append schedule'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'index.html', 'Expected formview.html')

    def test_home_admin_ChangeTaBio(self):
        response = self.c.post('/', {'command': 'change ta bio'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'index.html', 'Expected index.html')

    def test_home_admin_ChangePreferences(self):
        response = self.c.post('/', {'command': 'change preferences'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'index.html', 'Expected formview.html')

    def test_home_admin_ChangeInstructorBio(self):
        response = self.c.post('/', {'command': 'change instructor bio'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'index.html', 'Expected formview.html')

    def test_home_admin_EditSchedule(self):
        response = self.c.post('/', {'command': 'edit schedule'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'index.html', 'Expected formview.html')

    def test_home_admin_RemoveCourse(self):
        response = self.c.post('/', {'command': 'remove course'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'index.html', 'Expected index.html')

    def test_home_admin_RemoveLab(self):
        response = self.c.post('/', {'command': 'remove lab'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'index.html', 'Expected index.html')

    def test_home_admin_UnassignTa(self):
        response = self.c.post('/', {'command': 'unassign ta'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'formview.html', 'Expected formview.html')

    def test_home_admin_UnassignInstructor(self):
        response = self.c.post('/', {'command': 'unassign instructor'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'formview.html', 'Expected formview.html')

    def test_home_admin_ScheduleCourse(self):
        response = self.c.post('/', {'command': 'schedule course'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'index.html', 'Expected index.html')

    def test_home_admin_ScheduleLab(self):
        response = self.c.post('/', {'command': 'schedule lab'})
        self.assertEqual(response.status_code, 200)

    def test_home_admin_ViewMyInfo(self):
        response = self.c.post('/', {'command': 'view my info'})
        self.assertEqual(response.status_code, 200)

    def test_home_admin_logout(self):
        response = self.c.post('/', {'command': 'logout'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'formview.html', 'Expected formview.html')


class TestHome_instructor(TestCase):
    def setUp(self):
        self.c = Client()
        instructor = Instructor(email='inst', password='password')
        instructor.save()
        self.c.post('/login', {'email': 'inst', 'password': 'password'})

    def test_home_instructor_empty(self):
        response = self.c.post('/', {'command': ''})
        self.assertEqual(response.status_code, 200)

    def test_home_instructor_login(self):
        response = self.c.post('/', {'command': 'login'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'index.html', 'Expected index.html')
        self.assertEqual(response.context['message'], 'Invalid Command.')

    def test_home_instructor_addTA(self):
        response = self.c.post('/', {'command': 'add ta'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'index.html', 'Expected index.html')
        self.assertEqual(response.context['message'], 'Invalid Command.')

    def test_home_instructor_addCourse(self):
        response = self.c.post('/', {'command': 'add course'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'index.html', 'Expected index.html')
        self.assertEqual(response.context['message'], 'Invalid Command.')

    def test_home_instructor_addInstructor(self):
        response = self.c.post('/', {'command': 'add instructor'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'index.html', 'Expected index.html')
        self.assertEqual(response.context['message'], 'Invalid Command.')

    def test_home_instructor_addLab(self):
        response = self.c.post('/', {'command': 'add lab'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'index.html', 'Expected index.html')
        self.assertEqual(response.context['message'], 'Invalid Command.')

    def test_home_instructor_assignTA(self):
        response = self.c.post('/', {'command': 'assign ta'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'index.html', 'Expected index.html')
        self.assertEqual(response.context['message'], 'Invalid Command.')

    def test_home_instructor_assignInstructor(self):
        response = self.c.post('/', {'command': 'assign instructor'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'index.html', 'Expected index.html')
        self.assertEqual(response.context['message'], 'Invalid Command.')

    def test_home_instructor_changePassword(self):
        response = self.c.post('/', {'command': 'change password'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'formview.html', 'Expected formview.html')

    def test_home_instructor_AddLab(self):
        response = self.c.post('/', {'command': 'add lab'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name,  'index.html', 'Expected index.html')

    def test_home_instructor_DeleteUser(self):
        response = self.c.post('/', {'command': 'delete user'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name,  'index.html', 'Expected index.html')

    def test_home_instructor_AppendSchedule(self):
        response = self.c.post('/', {'command': 'append schedule'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'index.html', 'Expected formview.html')

    def test_home_instructor_ChangeTaBio(self):
        response = self.c.post('/', {'command': 'change ta bio'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'index.html', 'Expected index.html')

    def test_home_instructor_ChangePreferences(self):
        response = self.c.post('/', {'command': 'change preferences'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'index.html', 'Expected formview.html')

    def test_home_instructor_ChangeInstructorBio(self):
        response = self.c.post('/', {'command': 'change instructor bio'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'formview.html', 'Expected formview.html')

    def test_home_instructor_EditSchedule(self):
        response = self.c.post('/', {'command': 'edit schedule'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'index.html', 'Expected index.html')

    def test_home_instructor_RemoveCourse(self):
        response = self.c.post('/', {'command': 'remove course'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'index.html', 'Expected index.html')

    def test_home_instructor_RemoveLab(self):
        response = self.c.post('/', {'command': 'remove lab'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'index.html', 'Expected index.html')

    def test_home_instructor_UnassignTa(self):
        response = self.c.post('/', {'command': 'unassign ta'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name,  'index.html', 'Expected index.html')

    def test_home_instructor_UnassignInstructor(self):
        response = self.c.post('/', {'command': 'unassign instructor'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name,  'index.html', 'Expected index.html')

    def test_home_instructor_ScheduleCourse(self):
        response = self.c.post('/', {'command': 'schedule course'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'index.html', 'Expected index.html')

    def test_home_instructor_ScheduleLab(self):
        response = self.c.post('/', {'command': 'schedule lab'})
        self.assertEqual(response.status_code, 200)

    def test_home_instructor_ViewMyInfo(self):
        response = self.c.post('/', {'command': 'view my info'})
        self.assertEqual(response.status_code, 200)

    def test_home_instructor_logout(self):
        response = self.c.post('/', {'command': 'logout'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'formview.html', 'Expected formview.html')


class TestHome_ta(TestCase):
    def setUp(self):
        self.c = Client()
        ta = TA(email='ta', password='password')
        ta.save()
        Admin.add_ta(ta)
        self.c.post('/login', {'email': 'ta', 'password': 'password'})

    def test_home_ta_empty(self):
        response = self.c.post('/', {'command': ''})
        self.assertEqual(response.status_code, 200)

    def test_home_ta_login(self):
        response = self.c.post('/', {'command': 'login'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'index.html', 'Expected index.html')
        self.assertEqual(response.context['message'], 'Invalid Command.')

    def test_home_ta_addTA(self):
        response = self.c.post('/', {'command': 'add ta'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'index.html', 'Expected index.html')
        self.assertEqual(response.context['message'], 'Invalid Command.')

    def test_home_ta_addCourse(self):
        response = self.c.post('/', {'command': 'add course'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'index.html', 'Expected index.html')
        self.assertEqual(response.context['message'], 'Invalid Command.')

    def test_home_ta_addInstructor(self):
        response = self.c.post('/', {'command': 'add instructor'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'index.html', 'Expected index.html')
        self.assertEqual(response.context['message'], 'Invalid Command.')

    def test_home_ta_addLab(self):
        response = self.c.post('/', {'command': 'add lab'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'index.html', 'Expected index.html')
        self.assertEqual(response.context['message'], 'Invalid Command.')

    def test_home_ta_assignTA(self):
        response = self.c.post('/', {'command': 'assign ta'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'index.html', 'Expected index.html')
        self.assertEqual(response.context['message'], 'Invalid Command.')

    def test_home_ta_assignInstructor(self):
        response = self.c.post('/', {'command': 'assign instructor'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'index.html', 'Expected index.html')
        self.assertEqual(response.context['message'], 'Invalid Command.')

    def test_home_ta_changePassword(self):
        response = self.c.post('/', {'command': 'change password'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'formview.html', 'Expected formview.html')

    def test_home_ta_AddLab(self):
        response = self.c.post('/', {'command': 'add lab'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'index.html', 'Expected index.html')

    def test_home_ta_DeleteUser(self):
        response = self.c.post('/', {'command': 'delete user'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'index.html', 'Expected index.html')

    def test_home_ta_AppendSchedule(self):
        response = self.c.post('/', {'command': 'append schedule'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'index.html', 'Expected formview.html')

    def test_home_ta_ChangeTaBio(self):
        response = self.c.post('/', {'command': 'change ta bio'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name,'formview.html', 'Expected formview.html')

    def test_home_ta_ChangePreferences(self):
        response = self.c.post('/', {'command': 'change preferences'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'formview.html', 'Expected formview.html')

    def test_home_ta_ChangeInstructorBio(self):
        response = self.c.post('/', {'command': 'change instructor bio'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'index.html', 'Expected formview.html')

    def test_home_ta_EditSchedule(self):
        response = self.c.post('/', {'command': 'edit schedule'}, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'schedule.html', 'Expected schedule.html')

    def test_home_ta_RemoveCourse(self):
        response = self.c.post('/', {'command': 'remove course'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name,'formview.html', 'Expected formview.html')

    def test_home_ta_RemoveLab(self):
        response = self.c.post('/', {'command': 'remove lab'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name,'formview.html', 'Expected formview.html')

    def test_home_ta_UnassignTa(self):
        response = self.c.post('/', {'command': 'unassign ta'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name,  'index.html', 'Expected index.html')

    def test_home_ta_UnassignInstructor(self):
        response = self.c.post('/', {'command': 'unassign instructor'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name,  'index.html', 'Expected index.html')

    def test_home_ta_ScheduleCourse(self):
        response = self.c.post('/', {'command': 'schedule course'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name,'formview.html', 'Expected formview.html')

    def test_home_ta_ScheduleLab(self):
        response = self.c.post('/', {'command': 'schedule lab'})
        self.assertEqual(response.status_code, 200)

    def test_home_ta_ViewMyInfo(self):
        response = self.c.post('/', {'command': 'view my info'})
        self.assertEqual(response.status_code, 200)

    def test_home_ta_logout(self):
        response = self.c.post('/', {'command': 'logout'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'formview.html', 'Expected formview.html')


class TestLoginLogout(TestCase):
    def setUp(self):
        self.c = Client()

        self.admin = Admin(email='admin', password='password')
        self.admin.save()

        self.instructorA = Instructor(first_name='Prof. Allen', last_name='Abernathy', email='a@gmail.com', password='a123')
        self.instructorA.save()

        self.TaA = TA(first_name='Abby', last_name='Anderson', email='abby@gmail.com', password='a123', phone='000-000-0000', address='Ta A Dr.')
        self.TaA.save()

    def test_login_admin(self):
        response = self.c.post('/login', {'email': 'admin', 'password': 'password'})
        self.assertEqual(response.context['message'], 'Login Success')
        self.assertEqual(response.status_code, 200)
        self.assertIn('email', self.c.session.keys())
        self.assertEqual(self.c.session['email'], 'admin', 'Expected "Admin"')

    def test_login_instructor(self):
        response = self.c.post('/login', {'email': 'a@gmail.com', 'password': 'a123'})
        self.assertEqual(response.context['message'], 'Login Success')
        self.assertEqual(response.status_code, 200)
        self.assertIn('email', self.c.session.keys())
        self.assertEqual(self.c.session['email'], 'a@gmail.com', 'Expected "a@gmail.com"')

    def test_login_ta(self):
        response = self.c.post('/login', {'email': 'abby@gmail.com', 'password': 'a123'})
        self.assertEqual(response.context['message'], 'Login Success')
        self.assertEqual(response.status_code, 200)
        self.assertIn('email', self.c.session.keys())
        self.assertEqual(self.c.session['email'], 'abby@gmail.com', 'Expected "abby@gmail.com"')


class TestAddTA(TestCase):
    def setUp(self):
        self.c = Client()
        admin = Admin(email='admin', password='password')
        admin.save()
        self.c.post('/login', {'email': 'admin', 'password': 'password'})

    def test_AddTA(self):
        response = self.c.post('/form?add_ta', {'first_name': 'Abby', 'last_name': 'Jay', 'email': 'abby@gmail.com', 'password': TA.generate_password()})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(len(TA.objects.all()), 1)
        self.assertEqual(TA.objects.get(email='abby@gmail.com').first_name, 'Abby')


class TestAddInstructor(TestCase):
    def setUp(self):
        self.c = Client()
        admin = Admin(email='admin', password='password')
        admin.save()
        self.c.post('/login', {'email': 'admin', 'password': 'password'})

    def test_AddInstructor(self):
        response = self.c.post('/form?add_instructor', {'first_name': 'Bob', 'last_name': 'Jay', 'email': 'bob@gmail.com', 'password': Instructor.generate_password()})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(len(Instructor.objects.all()), 1)
        self.assertEqual(Instructor.objects.get(email='bob@gmail.com').first_name, 'Bob')


class TestAddCourse(TestCase):
    def setUp(self):
        self.c = Client()
        admin = Admin(email='admin', password='password')
        admin.save()
        self.c.post('/login', {'email': 'admin', 'password': 'password'})

    def test_AddCourse(self):
        response = self.c.post('/form?add_course', {'course_name': 'CS250', 'days': ["M", "W"], 'start': time(10, 00), 'end': time(11, 00)})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(len(Course.objects.all()), 1)
        self.assertEqual(Course.objects.get(course_name='CS250').course_name, 'CS250')


class TestAddLab(TestCase):
    def setUp(self):
        self.c = Client()
        admin = Admin(email='admin', password='password')
        admin.save()
        self.c.post('/login', {'email': 'admin', 'password': 'password'})

        self.timeA = CourseTime(days='MW', start='08:00', end='10:45')
        self.timeA.save()

        self.courseA = Course(course_name='CS250', lecture_time=self.timeA)
        self.courseA.save()

    def test_AddLab(self):
        response = self.c.post('/form?add_lab', {'Course': [str(self.courseA)], 'lab_number': ['2'], 'days': ["M", "W"], 'start': time(14, 00), 'end': time(14, 50)})
        self.assertEqual(response.status_code, 200)


class TestAssignTACourse(TestCase):
    def setUp(self):
        self.c = Client()
        admin = Admin(email='admin', password='password')
        admin.save()
        self.c.post('/login', {'email': 'admin', 'password': 'password'})

        self.instructorA = Instructor(first_name='Prof. Allen', last_name='Abernathy', email='a@gmail.com', password='a123')
        self.instructorA.save()

        self.timeA = CourseTime(days='MW', start='08:00', end='10:45')
        self.timeA.save()

        self.TaA = TA(first_name='Abby', last_name='Anderson', email='abby@gmail.com', password='a123', phone='000-000-0000', address='Ta A Dr.')
        self.TaA.save()
        admin.add_ta(self.TaA)
        self.TaB = TA(first_name='Bill', last_name='Bronson', email='bill@gmail.com', password='b123', phone='000-000-0001', address='Ta B Dr.')
        self.TaB.save()
        admin.add_ta(self.TaB)
        self.TaC = TA(first_name='Chloe', last_name='Christopherson', email='chloe@gmail.com', password='c123', phone='000-000-0002', address='Ta C Dr.')
        self.TaC.save()
        admin.add_ta(self.TaC)

        self.courseA = Course(course_name='CS250', lecture_time=self.timeA)
        self.courseA.save()

    def test_AssignTACourse_proper(self):
        response = self.c.post('/form?assign_ta', {'ta': [self.TaA.email], 'course': [str(self.courseA)]})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Course.objects.get(course_name="CS250").get_ta(0), TA.objects.get(email="abby@gmail.com"), "TA not set in class properly.")

    def test_AssignTACourse_improper_doubleAssign(self):
        response = self.c.post('/form?assign_ta', {'ta': [self.TaA.email], 'course': [str(self.courseA)]})
        self.assertEqual(response.status_code, 302)
        response = self.c.post('/form?assign_ta', {'ta': [self.TaA.email], 'course': [str(self.courseA)]})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['form'].non_field_errors()[0], 'Course already has this TA')


class TestAssignTALab(TestCase):
    def setUp(self):
        self.c = Client()
        admin = Admin(email='admin', password='password')
        admin.save()
        self.c.post('/login', {'email': 'admin', 'password': 'password'})

        self.instructorA = Instructor(first_name='Prof. Allen', last_name='Abernathy', email='a@gmail.com', password='a123')
        self.instructorA.save()

        self.TaA = TA(first_name='Abby', last_name='Anderson', email='abby@gmail.com', password='a123', phone='000-000-0000', address='Ta A Dr.')
        self.TaA.save()
        admin.add_ta(self.TaA)
        self.TaB = TA(first_name='Bill', last_name='Bronson', email='bill@gmail.com', password='b123', phone='000-000-0001', address='Ta B Dr.')
        self.TaB.save()
        admin.add_ta(self.TaA)

        self.timeA = CourseTime(days='MW', start=time(10, 00), end=time(12, 30))
        self.timeA.save()

        self.timeB = CourseTime(days='TR', start=time(10, 00), end=time(12, 30))
        self.timeB.save()

        self.courseA = Course(course_name='CS250', lecture_time=self.timeA)
        self.courseA.save()

        self.labA = Lab(lab_number=1, time=self.timeB)
        self.labA.save()
        self.courseA.add_lab(self.labA)

    def test_AssignTALab_proper(self):
        response = self.c.post('/form?assign_ta', {'ta': [self.TaA.email], 'course': [str(self.courseA)], 'lab': [str(self.labA.lab_number)]})
        self.assertEqual(response.status_code, 302)
        self.assertEqual((Lab.objects.get(lab_number="001").get_ta(0)), TA.objects.get(email="abby@gmail.com"), "TA not set in class properly.")

    def test_AssignTALab_improper_doubleAssign(self):
        response = self.c.post('/form?assign_ta', {'ta': [self.TaA.email], 'course': [str(self.courseA)], 'lab': [str(self.labA.lab_number)]})
        self.assertEqual(response.status_code, 302)
        response = self.c.post('/form?assign_ta', {'ta': [self.TaA.email], 'course': [str(self.courseA)], 'lab': [str(self.labA.lab_number)]})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['form'].non_field_errors()[0], 'Course already has this TA')


class TestAssignInstructor(TestCase):
    def setUp(self):
        self.c = Client()
        admin = Admin(email='admin', password='password')
        admin.save()
        self.c.post('/login', {'email': 'admin', 'password': 'password'})

        self.instructorA = Instructor(first_name='Prof. Allen', last_name='Abernathy', email='a@gmail.com', password='a123')
        self.instructorA.save()
        self.instructorB = Instructor(first_name='Prof. Bob', last_name='Benson', email='b@gmail.com', password='b123')
        self.instructorB.save()

        self.timeA = CourseTime(days='MW', start=time(10, 00), end=time(12, 30))
        self.timeA.save()

        self.courseA = Course(course_name='CS250', lecture_time=self.timeA)
        self.courseA.save()

    def test_AssignInstructor_proper(self):
        response = self.c.post('/form?assign_instructor', {'instructor': [self.instructorA.email], 'course': [str(self.courseA)]})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Course.objects.get(course_name='CS250').get_instructor(0), Instructor.objects.get(email="a@gmail.com"), "Instructor not set in class properly.")

    def test_AssignInstructor_improper_doubleAssign(self):
        response = self.c.post('/form?assign_instructor', {'instructor': [self.instructorA.email], 'course': [str(self.courseA)]})
        self.assertEqual(response.status_code, 302)
        response = self.c.post('/form?assign_instructor', {'instructor': [self.instructorA.email], 'course': [str(self.courseA)]})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['form'].non_field_errors()[0], 'Course already has this Instructor')


class TestListAdmin(TestCase):
    def setUp(self):
        self.c = Client()
        self.admin = Admin(email='admin', password='password')
        self.admin.save()
        self.c.post('/login', {'email': 'admin', 'password': 'password'})

        self.instructorA = Instructor(first_name='Prof. Allen', last_name='Abernathy', email='a@gmail.com', password='a123')
        self.instructorA.save()
        self.instructorB = Instructor(first_name='Prof. Bob', last_name='Benson', email='b@gmail.com', password='b123')
        self.instructorB.save()
        self.instructorC = Instructor(first_name='Prof. Clyde', last_name='Cisdale', email='c@gmail.com', password='c123')
        self.instructorC.save()

        self.TaA = TA(first_name='Abby', last_name='Anderson', email='abby@gmail.com', password='a123', phone='000-000-0000', address='Ta A Dr.')
        self.TaA.save()
        self.TaB = TA(first_name='Bill', last_name='Bronson', email='bill@gmail.com', password='b123', phone='000-000-0001', address='Ta B Dr.')
        self.TaB.save()
        self.TaC = TA(first_name='Chloe', last_name='Christopherson', email='chloe@gmail.com', password='c123', phone='000-000-0002', address='Ta C Dr.')
        self.TaC.save()

        self.timeA = CourseTime(days='TR', start=time(10, 00), end=time(12, 30))
        self.timeA.save()

        self.timeB = CourseTime(days='MW', start=time(10, 00), end=time(12, 30))
        self.timeB.save()

        self.timeC = CourseTime(days='F', start=time(10, 00), end=time(12, 30))
        self.timeC.save()

        self.timeD = CourseTime(days='MTW', start=time(16, 00), end=time(18, 30))
        self.timeD.save()

        self.courseA = Course(course_name='CS250', lecture_time=self.timeA)
        self.courseA.save()

        self.courseB = Course(course_name='CS251', lecture_time=self.timeB)
        self.courseB.save()
        self.courseB.set_ta(self.TaA)

        self.courseC = Course(course_name='CS351', lecture_time=self.timeC)
        self.courseC.save()
        self.courseC.set_instructor(self.instructorA)

        self.courseD = Course(course_name='CS352', lecture_time=self.timeD)
        self.courseD.save()

    def test_ListTA(self):
        response = self.c.post('/', {'command': 'list tas'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'displayTA.html', 'Expected displayTA.html')
        self.assertEqual(TA.objects.all, response.context["message"], "initial out improper string")

    def test_ListInstructors(self):
        response = self.c.post('/', {'command': 'list instructors'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'displayInstructor.html', 'Expected displayInstructor.html')
        self.assertEqual(Instructor.objects.all, response.context["message"], "initial out improper string")

    def test_ListCourse(self):
        response = self.c.post('/', {'command': 'list courses'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'displaycourse.html', 'Expected displaycourse.html')
        self.assertEqual(Course.objects.all, response.context["message"], "initial out improper string")


class TestListInstructor(TestCase):
    def setUp(self):
        self.c = Client()
        self.instructor = Instructor(email='instructor', password='password')
        self.instructor.save()
        self.c.post('/login', {'email': 'instructor', 'password': 'password'})

        self.instructorA = Instructor(first_name='Prof. Allen', last_name='Abernathy', email='a@gmail.com', password='a123')
        self.instructorA.save()
        self.instructorB = Instructor(first_name='Prof. Bob', last_name='Benson', email='b@gmail.com', password='b123')
        self.instructorB.save()
        self.instructorC = Instructor(first_name='Prof. Clyde', last_name='Cisdale', email='c@gmail.com', password='c123')
        self.instructorC.save()

        self.TaA = TA(first_name='Abby', last_name='Anderson', email='abby@gmail.com', password='a123', phone='000-000-0000', address='Ta A Dr.')
        self.TaA.save()
        self.TaB = TA(first_name='Bill', last_name='Bronson', email='bill@gmail.com', password='b123', phone='000-000-0001', address='Ta B Dr.')
        self.TaB.save()
        self.TaC = TA(first_name='Chloe', last_name='Christopherson', email='chloe@gmail.com', password='c123', phone='000-000-0002', address='Ta C Dr.')
        self.TaC.save()

        self.timeA = CourseTime(days='TR', start=time(10, 00), end=time(12, 30))
        self.timeA.save()

        self.timeB = CourseTime(days='MW', start=time(10, 00), end=time(12, 30))
        self.timeB.save()

        self.timeC = CourseTime(days='F', start=time(10, 00), end=time(12, 30))
        self.timeC.save()

        self.timeD = CourseTime(days='MTW', start=time(16, 00), end=time(18, 30))
        self.timeD.save()

        self.courseA = Course(course_name='CS250', lecture_time=self.timeA)
        self.courseA.save()

        self.courseB = Course(course_name='CS251', lecture_time=self.timeB)
        self.courseB.save()
        self.courseB.set_ta(self.TaA)

        self.courseC = Course(course_name='CS351', lecture_time=self.timeC)
        self.courseC.save()
        self.courseC.set_instructor(self.instructorA)

        self.courseD = Course(course_name='CS352', lecture_time=self.timeD)
        self.courseD.save()

    def test_ListTA(self):
        response = self.c.post('/', {'command': 'list tas'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'displayTA.html', 'Expected displayTA.html')
        self.assertEqual(TA.objects.all, response.context["message"], "initial out improper string")

    def test_ListInstructors(self):
        response = self.c.post('/', {'command': 'list instructors'})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.templates[0].name, 'displayInstructor.html', 'Expected displayInstructor.html')
        self.assertEqual(Instructor.objects.all, response.context["message"], "initial out improper string")

    def test_ListCourse(self):
        response = self.c.post('/', {'command': 'list courses'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'displaycourse.html', 'Expected displaycourse.html')
        self.assertEqual(Course.objects.all, response.context["message"], "initial out improper string")


class TestListTA(TestCase):
    def setUp(self):
        self.c = Client()
        self.ta = TA(email='ta', password='password')
        self.ta.save()
        self.c.post('/login', {'email': 'ta', 'password': 'password'})

        self.instructorA = Instructor(first_name='Prof. Allen', last_name='Abernathy', email='a@gmail.com', password='a123')
        self.instructorA.save()
        self.instructorB = Instructor(first_name='Prof. Bob', last_name='Benson', email='b@gmail.com', password='b123')
        self.instructorB.save()
        self.instructorC = Instructor(first_name='Prof. Clyde', last_name='Cisdale', email='c@gmail.com', password='c123')
        self.instructorC.save()

        self.TaA = TA(first_name='Abby', last_name='Anderson', email='abby@gmail.com', password='a123', phone='000-000-0000', address='Ta A Dr.')
        self.TaA.save()
        self.TaB = TA(first_name='Bill', last_name='Bronson', email='bill@gmail.com', password='b123', phone='000-000-0001', address='Ta B Dr.')
        self.TaB.save()
        self.TaC = TA(first_name='Chloe', last_name='Christopherson', email='chloe@gmail.com', password='c123', phone='000-000-0002', address='Ta C Dr.')
        self.TaC.save()

        self.timeA = CourseTime(days='TR', start=time(10, 00), end=time(12, 30))
        self.timeA.save()

        self.timeB = CourseTime(days='MW', start=time(10, 00), end=time(12, 30))
        self.timeB.save()

        self.timeC = CourseTime(days='F', start=time(10, 00), end=time(12, 30))
        self.timeC.save()

        self.timeD = CourseTime(days='MTW', start=time(16, 00), end=time(18, 30))
        self.timeD.save()

        self.courseA = Course(course_name='CS250', lecture_time=self.timeA)
        self.courseA.save()

        self.courseB = Course(course_name='CS251', lecture_time=self.timeB)
        self.courseB.save()
        self.courseB.set_ta(self.TaA)

        self.courseC = Course(course_name='CS351', lecture_time=self.timeC)
        self.courseC.save()
        self.courseC.set_instructor(self.instructorA)

        self.courseD = Course(course_name='CS352', lecture_time=self.timeD)
        self.courseD.save()

    def test_ListTA(self):
        response = self.c.post('/', {'command': 'list tas'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'displayTA.html', 'Expected displayTA.html')
        self.assertEqual(TA.objects.all, response.context["message"], "initial out improper string")

    def test_ListInstructors(self):
        response = self.c.post('/', {'command': 'list instructors'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'displayInstructor.html', 'Expected displayInstructor.html')
        self.assertEqual(Instructor.objects.all, response.context["message"], "initial out improper string")

    def test_ListCourse(self):
        response = self.c.post('/', {'command': 'list courses'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'displaycourse.html', 'Expected displaycourse.html')
        self.assertEqual(Course.objects.all, response.context["message"], "initial out improper string")


class TestTime(TestCase):
    def setUp(self):
        self.c = Client()
        self.admin = Admin(email='admin', password='password')
        self.admin.save()
        self.c.post('/login', {'email': 'admin', 'password': 'password'})

        self.timeA = CourseTime(days='MW', start=time(10, 00), end=time(10, 50))
        self.timeA.save()

        self.timeB = CourseTime(days='TR', start=time(14, 00), end=time(14, 50))
        self.timeB.save()

        self.taA = TA(email='ta', password='password')
        self.taA.save()

        self.instructorA = Instructor(first_name='Prof. Allen', last_name='Abernathy', email='a@gmail.com', password='a123')
        self.instructorA.save()

        self.labA = Lab(lab_number='001', time=self.timeA)
        self.labA.save()

        self.courseA = Course(course_name='CS250', lecture_time=self.timeA)
        self.courseA.save()

    def test_LabStart(self):
        self.assertEqual(self.labA.time.start, time(10, 00), 'Expected a Lab Start Time of 10:00am')

    def test_LabEnd(self):
        self.assertEqual(self.labA.time.end, time(10, 50), 'Expected a Lab End Time of 10:50am')

    def test_LabDays(self):
        self.assertEqual(self.labA.time.days, 'MW', 'Expected Lab Days "MW"')

    def test_CourseStart(self):
        self.assertEqual(self.courseA.lecture_time.start, time(10, 00), 'Expected a Lecture Start Time of 10:00am')

    def test_CourseEnd(self):
        self.assertEqual(self.courseA.lecture_time.end, time(10, 50), 'Expected a Lecture End Time of 10:00am')

    def test_CourseDays(self):
        self.assertEqual(self.courseA.lecture_time.days, 'MW', 'Expected Course Days "MW"')

    def test_LabSetTime(self):
        self.labA.set_time(self.timeB)
        self.assertEqual(self.labA.time.start, time(14, 00), 'Expected a Lab Start Time of 2:00pm')
        self.assertEqual(self.labA.time.end, time(14, 50), 'Expected a Lab End Time of 2:50pm')
        self.assertEqual(self.labA.time.days, 'TR', 'Expected Lab Days "TR"')

    def test_CourseSetTime(self):
        self.courseA.set_lecture_time(self.timeB)
        self.assertEqual(self.courseA.lecture_time.start, time(14, 00), 'Expected a Course Start Time of 2:00pm')
        self.assertEqual(self.courseA.lecture_time.end, time(14, 50), 'Expected a Course End Time of 2:50pm')
        self.assertEqual(self.courseA.lecture_time.days, 'TR', 'Expected Course Days "TR"')


class ChangePasswordInstructor(TestCase):
    def setUp(self):
        self.c = Client()
        self.instructor = Instructor(email='instructor', password='password')
        self.instructor.save()
        self.c.post('/login', {'email': 'instructor', 'password': 'password'})

    def test_ChangePasswordProper_Instructor(self):
        response = self.c.post('/form?change_password', {'new_password1': 'pass1', 'new_password2': 'pass'})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Instructor.objects.get(email='instructor').password, 'pass1', 'Expected Password to change to: pass1')

    def test_ChangePasswordDifferentStrings_Instructor(self):
        response = self.c.post('/form?change_password', {'new_password1': 'pass1', 'new_password2': 'ksgkdg'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['form'].non_field_errors()[0], 'Passwords Do Not Match')

class ChangePasswordTA(TestCase):
    def setUp(self):
        self.c = Client()
        self.ta = TA(email='ta', password='password')
        self.ta.save()
        self.c.post('/login', {'email': 'ta', 'password': 'password'})

    def test_ChangePasswordProper_Instructor(self):
        response = self.c.post('/form?change_password', {'new_password1': 'pass1', 'new_password2': 'pass1'})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(TA.objects.get(email='ta').password, 'pass1', 'Expected Password to change to: pass1')

    def test_ChangePasswordDifferentStrings_Instructor(self):
        response = self.c.post('/form?change_password', {'new_password1': 'pass1', 'new_password2': 'ksgkdg'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['form'].non_field_errors()[0], 'Passwords Do Not Match')
