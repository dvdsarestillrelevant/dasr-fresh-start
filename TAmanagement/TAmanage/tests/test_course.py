# Justin Franecki
from ..models.course_model import Course
from ..models.course_time_model import CourseTime
from ..models.ta_model import TA
from django.test import TestCase
from datetime import time


class TestCourse(TestCase):
    def setUp(self):
        self.timeA = CourseTime(days='MW', start=time(10, 00), end=time(10, 50))
        self.timeA.save()
        self.timeB = CourseTime(days='F', start=time(11, 00), end=time(12, 50))
        self.timeB.save()
        self.c1 = Course(course_name="name", lecture_time=self.timeA)
        self.c1.save()
        self.assertEqual(Course.objects.filter(course_name="name")[0], self.c1)
        self.c2 = Course(course_name=43, lecture_time=self.timeB)
        self.c2.save()

    def test_create(self):
        self.assertIsInstance(Course.objects.filter(course_name="name")[0].course_name, str, "Course Name must be a string")
        self.assertNotIsInstance(Course.objects.filter(course_name=43)[0].course_name, int, "Course Name cannot be an int")

    def test_setters(self):
        self.c1 = Course.objects.filter(course_name="name")[0]
        self.assertEqual(Course.objects.filter(course_name="name")[0], self.c1)
        self.ta1 = TA(first_name="joe", email="joe@uwm.edu")
        self.ta1.save()
        self.c1.set_ta(self.ta1)
        self.c1.save()
        self.assertEqual(Course.objects.filter(course_name="name")[0].ta.filter(first_name="joe")[0], self.ta1)

    def test_getters(self):
        self.c1 = Course.objects.filter(course_name="name")[0]
        self.assertEqual(Course.objects.filter(course_name="name")[0], self.c1)
        self.assertEqual(Course.objects.filter(course_name="name")[0].course_name, "name")
        self.ta1 = TA(first_name="bob", email="bob@uwm.edu")
        self.ta1.save()
        self.c1.ta.add(self.ta1)
        self.c1.save()
        self.assertEqual(Course.objects.filter(course_name="name")[0].get_ta(0), self.ta1)
