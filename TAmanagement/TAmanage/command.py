from .forms.add_ta import TAForm
from .forms.add_instructor import InstructorForm
from .forms.add_course import CourseForm
from .forms.assign_ta import TaCourse
from .forms.assign_instructor import InstructorCourse
from .forms.change_password import ChangePasswordForm
from .forms.add_lab import LabForm
from .forms.delete_form import DeleteUser
from .forms.Ta_schedule import Schedule
from .forms.bio_form import BioForm
from .forms.change_preferences import ChangePreferencesForm
from .forms.unassign_ta_form import UnassignTAForm
from .forms.unassign_instructor_form import UnassignInstructorForm
from .forms.add_schedule_course import ScheduleCourse
from .forms.add_schedule_lab import ScheduleLab
from .forms.remove_course import RemoveCourse
from .forms.remove_lab import RemoveLab


class Command:
    def __init__(self, form, action):
        self.form = form
        self.action = action

    def process(self, user, *args):
        if self.form:
            filled_form = self.form(*args)
            if filled_form.is_valid():
                if hasattr(filled_form, "save"):
                    model = filled_form.save()
                    getattr(user, self.action)(model)
                else:
                    getattr(user, self.action)(*filled_form.cleaned_data.values())
                return False
            return filled_form
        return getattr(user, self.action)(*args)

    def check_command(self, user):
        return hasattr(user, self.action)


class Driver:
    # Defining the commands that will be used for our driver
    logout_command = Command(None, "logout")
    add_ta_command = Command(TAForm, "add_ta")
    add_instr_command = Command(InstructorForm, "add_instructor")
    add_course_command = Command(CourseForm, "add_course")
    assign_ta_command = Command(TaCourse, "assign_ta")
    assign_instr_command = Command(InstructorCourse, "assign_instructor")
    list_tas_command = Command(None, "list_tas")
    list_instructors_command = Command(None, "list_instructors")
    display_course_command = Command(None, "display_course")
    change_password_command = Command(ChangePasswordForm, "change_password")
    add_lab_command = Command(LabForm, "add_lab")
    delete_user_command = Command(DeleteUser, "delete_user")
    append_ta_schedule = Command(Schedule, "append_schedule")
    change_ta_bio_command = Command(BioForm, "change_ta_bio")
    change_preferences_command = Command(ChangePreferencesForm, "change_preferences")
    change_instructor_bio_command = Command(BioForm, "change_instructor_bio")
    unassign_ta_command = Command(UnassignTAForm, "unassign_ta")
    unassign_instructor_command = Command(UnassignInstructorForm, "unassign_instructor")
    edit_schedule_command = Command(None, "edit_schedule")
    schedule_course_command = Command(ScheduleCourse, "schedule_course")
    schedule_course_lab = Command(ScheduleLab, "schedule_lab")
    schedule_remove_course = Command(RemoveCourse, "remove_course")
    schedule_remove_lab = Command(RemoveLab, "remove_lab")
    view_my_info = Command(None, "view_my_info")

    # Creating a dictionary of our commands to make it become easier to read/access
    commands = {
                'logout': logout_command,
                'add ta': add_ta_command,
                'add instructor': add_instr_command,
                'add course': add_course_command,
                'assign ta': assign_ta_command,
                'assign instructor': assign_instr_command,
                'list tas': list_tas_command,
                'list instructors': list_instructors_command,
                'list courses': display_course_command,
                'change password': change_password_command,
                'add lab': add_lab_command,
                'delete user': delete_user_command,
                'append schedule': append_ta_schedule,
                'change ta bio': change_ta_bio_command,
                'change preferences': change_preferences_command,
                'change instructor bio': change_instructor_bio_command,
                'edit schedule': edit_schedule_command,
                'remove course': schedule_remove_course,
                'remove lab': schedule_remove_lab,
                'unassign ta': unassign_ta_command,
                'unassign instructor': unassign_instructor_command,
                'schedule course': schedule_course_command,
                'schedule lab': schedule_course_lab,
                'view my info': view_my_info,
                }

    @staticmethod
    def main(in_str):
        if in_str in Driver.commands.keys():
            return Driver.commands[in_str]
        else:
            return "Command not found"
