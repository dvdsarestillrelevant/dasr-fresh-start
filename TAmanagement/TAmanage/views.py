from django.shortcuts import render, redirect
from django.views import View
from .command import Driver
from .command import Command
from .forms.login import LoginForm
from .forms.query import Query
from .models.course_model import Course
from .models.ta_model import TA
from .models.instructor_model import Instructor
from .models.schedule_model import Schedule
from .forms.add_schedule_course import ScheduleCourse
from .forms.add_schedule_lab import ScheduleLab


# Create your views here.
def course_view(request):
    context = {"message": Course.objects.all}
    return render(request, "displaycourse.html", context=context)


def ta_view(request):
    context = {"message": TA.objects.all}
    return render(request, "displayTA.html", context=context)


def instr_view(request):
    context = {"message": Instructor.objects.all}
    return render(request, "displayInstructor.html", context=context)


def myinfoview(request):
    user = get_current_user(request)
    context = {"user": user, "schedule": Schedule.objects.get(ta=user)}
    return render(request, "viewmyinfo.html", context)


funcs = {Course: course_view,
         TA: ta_view,
         Instructor: instr_view,
         "my_info": myinfoview
         }


# This is the default view
class Home(View):
    def get(self, request):
        if "email" not in request.session.keys():
            return render(request, "formview.html", {"form": LoginForm, "action": "/login"})
        context = {"message": ""}
        # This is a neat trick to make things play nice with redirection.
        # "\" == "\?submitted" according to requests
        if request.get_full_path().find("submitted") > -1:
            context.update({"message": "Form Submitted"})
        return render(request, "index.html", context)

    def post(self, request):
        context = {"message": "Invalid Command."}
        destination = "index.html"
        if "email" not in request.session.keys():
            return render(request, "formview.html", {"form": LoginForm, "action": "/login"})

        command_input = request.POST["command"]
        if command_input:
            command_input = command_input.lower()
            # Yeah
            # login magic...
            #   since login is a stupid thing it doesn't need to be registered as a command

            if command_input == "logout" and "email" in request.session.keys():
                request.session.pop("email")
                return render(request, "formview.html", {"form": LoginForm, "action": "/login"})
            elif request.session["type"] == "TA" and command_input == "edit schedule":
                return redirect("/schedule_course")
            else:
                # response just returns a command, instead of passing Driver as an arg,
                #   user would be the more applicable approach.
                response = Driver.main(command_input)
                user = get_current_user(request)
                if type(response) is Command and response.check_command(user):
                    if response.form:
                        # form is the form to prompt for argument input
                        # action is the action the command does pretty simple
                        context.update({"form": response.form(), "action": "/form?" + response.action})
                        destination = "formview.html"
                    else:
                        # this is processing the command, with User
                        message = response.process(user)

                        if message in funcs.keys():
                            return funcs[message](request)
                        # send out the newly determined message
                        context.update({"message": message})
        return render(request, destination, context=context)


class LabView(View):
    def get(self, request):
        context = {}
        path = request.get_full_path()
        course_name = path[path.find("?") + 1:].replace("_", " ")
        course = Course.objects.filter(course_name=course_name)
        if len(course) == 1:
            context.update({"course": course[0]})
        return render(request, "displaylab.html", context)

    def post(self, request):
        return self.get(request)


class TAView(View):
    def get(self, request):
        context = {}
        path = request.get_full_path()
        ta_email = path[path.find("?") + 1:].replace("_", " ")
        ta = TA.objects.filter(email=ta_email)
        if len(ta) == 1:
            context.update({"user": ta[0], "schedule": Schedule.objects.get(ta=ta[0])})
        return render(request, "viewmyinfo.html", context)

    def post(self, request):
        return self.get(request)


# this is basically a parent View that the next two inherit for ease.
class FormView(View):
    def get(self, request):
        # redirect to main page
        return redirect("/")

    def post(self, request):
        path = request.get_full_path()
        command = path[path.find("?") + 1:].replace("_", " ")
        user = get_current_user(request)
        failed = Driver.commands[command].process(user, request.POST)
        if failed:
            return render(request, "formview.html", {"form": failed})
        return redirect("/?submitted")


class ScheduleView(View):

    def get(self, request):
        context = {}
        schedule = Schedule.objects.get(ta=get_current_user(request))
        if schedule:
            context.update({"schedule": schedule, "schedulecourseform": ScheduleCourse, "schedulelabform": ScheduleLab})
        return render(request, "schedule.html", context)

    def post(self, request):
        context = {}
        user = get_current_user(request)
        schedule = Schedule.objects.get(ta=user)
        command_input = request.POST
        course_form = ScheduleCourse()
        lab_form = ScheduleLab()
        if "course" in command_input.keys():
            course_form = ScheduleCourse(command_input)
        if "lab" in command_input.keys():
            lab_form = ScheduleLab(command_input)
        if course_form.is_valid():
            user.schedule_course(course_form.cleaned_data.get("course"))
            course_form = ScheduleCourse()
        if lab_form.is_valid():
            user.schedule_lab(lab_form.cleaned_data.get("lab"))
            lab_form = ScheduleLab()

        if "del_course" in command_input.keys():
            user.remove_course(command_input["del_course"])
            
        if schedule:
            context.update({"schedule": schedule, "schedulecourseform": course_form, "schedulelabform":lab_form})
        return render(request, "schedule.html", context)


class Login(View):
    def get(self, request):
        if "email" not in request.session.keys():
            return render(request, "formview.html", {"form": LoginForm, "action": "/login"})
        else:
            return render(request, "index.html", {"message": "Already Logged in"})

    def post(self, request):
        # fill out form
        form = LoginForm(request.POST)
        # check if form is valid
        if form.is_valid():
            email = form.cleaned_data.get("email")
            password = form.cleaned_data.get("password")
            request.session["type"] = type(Query.get(email=email, password=password)).__name__
            request.session["email"] = form.cleaned_data.get("email")
            return render(request, "index.html", {"message": "Login Success"})
        return render(request, "formview.html", {"form": form, "action": "/login"})


class Logout(View):
    def post(self, request):
        del request.session["email"]
        return render(request, "index.html", {"message": "Logged Out"})


def get_current_user(request):
    if "email" not in request.session.keys():
        return None
    return Query.get(email=request.session["email"])

# def add_schedule(request):
#     if request.method == "POST":
#         form = ScheduleCourse(request.POST)
#         if form.is_valid():
#             return render(request, "taschedule.html", {"form": form})
#         else:
#             form = ScheduleCourse()
#     return render(request, "taschedule.html", {"form": form})
#
# def add_lab_schedule(request):
#     if request.method == "POST":
#         form = ScheduleLab(request.POST)
#         if form.is_valid():
#             return render(request, "taschedule.html", {"courseform": form})
#         else:
#             form = ScheduleLab()
#     return render(request, "taschedule.html", {"labform": form})