from django.contrib import admin
# this is how one would import a model from models folder
from .models.course_model import Course
# Register your models here.
admin.site.register(Course)
