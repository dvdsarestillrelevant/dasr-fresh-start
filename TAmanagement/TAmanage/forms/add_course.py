from django import forms
from ..models.course_model import Course
from ..models.course_time_model import CourseTime

class CourseForm(forms.Form):
    course_name = forms.CharField(label="Course Name:")
    choices = [('M', 'Monday'), ('T', 'Tuesday'), ('W', 'Wednesday'), ('R', 'Thursday'), ('F', 'Friday'),
               ('Sa', "Saturday")]
    days = forms.MultipleChoiceField(label="Days:", choices=choices, widget=forms.CheckboxSelectMultiple, required=True)
    start = forms.TimeField(label="Start Time",)
    end = forms.TimeField(label="End Time")

    def clean(self):
        cleaned_data = super().clean()
        name = cleaned_data.get("course_name")
        days = cleaned_data.get("days")
        start = cleaned_data.get("start")
        end = cleaned_data.get("end")

        if name and days and start and end:
            filter_name = Course.objects.filter(course_name=name)

            if len(filter_name) == 1:
                raise forms.ValidationError("Course name is invalid.")
            if start >= end:
                raise forms.ValidationError("End time is before start time")
            # Validate days
            cleaned_data.update({"days": "".join(days)})
        return cleaned_data
