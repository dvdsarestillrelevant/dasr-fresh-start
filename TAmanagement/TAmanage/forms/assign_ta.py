from django import forms
from ..models.ta_model import TA
from ..models.course_model import Course
from ..models.lab_model import Lab
from ..models.schedule_model import Schedule


class TaCourse(forms.Form):
    ta = forms.ModelChoiceField(queryset=TA.objects.all(), to_field_name="email")
    course = forms.ModelChoiceField(queryset=Course.objects.all(), to_field_name="course_name", required=False)
    lab = forms.ModelChoiceField(queryset=Lab.objects.all(), to_field_name="lab_number", required=False)
    # The clean function helps validate that the fields are good.
    # In this case, it validates that the given usernames belong to existing users and that there are no faults.



    def clean(self):
        cleaned_data = super().clean()
        ta = cleaned_data.get("ta")
        course = cleaned_data.get("course")
        lab = cleaned_data.get("lab")

        if lab and not course:
            course = lab.get_course()
            cleaned_data.update({"course": course})

        if ta and course:
            schedule = Schedule.objects.get(ta=ta)
            if len(course.ta.filter(email=ta.email)) >= 1:
                raise forms.ValidationError("Course already has this TA")
            if lab and len(course.labs.filter(lab_number=lab.lab_number)) < 1:
                raise forms.ValidationError("Lab does not exist")

            # Course conflict detection
            for c in schedule.course.all():
                if c.lecture_time.does_overlap(course.lecture_time):
                    raise forms.ValidationError("Course lecture conflicts with TA's schedule")
            for l in schedule.lab.all():
                if l.time.does_overlap(course.lecture_time):
                    raise forms.ValidationError("Course lecture conflicts with TA's schedule")

        if ta and lab:
            schedule = Schedule.objects.get(ta=ta)
            if lab.ta and lab.ta.email == ta.email:
                raise forms.ValidationError("Lab already has this TA")
            for c in schedule.course.all():
                if c.lecture_time.does_overlap(lab.time):
                    raise forms.ValidationError("Lab lecture conflicts with TA's schedule")
            for l in schedule.lab.all():
                if l.lecture_time.does_overlap(lab.time):
                    raise forms.ValidationError("Lab lecture conflicts with TA's schedule")
        if not course and not lab:
            raise forms.ValidationError("Select a lab or course")

        return cleaned_data

