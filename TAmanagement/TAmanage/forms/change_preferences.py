from django import forms
from ..models.course_model import Course


class ChangePreferencesForm(forms.Form):
    preference1 = forms.ModelChoiceField(queryset=Course.objects.all(), to_field_name="course_name", required=False, empty_label="None")
    preference2 = forms.ModelChoiceField(queryset=Course.objects.all(), to_field_name="course_name", required=False, empty_label="None")
    preference3 = forms.ModelChoiceField(queryset=Course.objects.all(), to_field_name="course_name", required=False, empty_label="None")

    def clean(self):
        cleaned_data = super().clean()
        pref1 = cleaned_data.get("preference1")
        pref2 = cleaned_data.get("preference2")
        pref3 = cleaned_data.get("preference3")

        if pref3 and not pref2:
            raise forms.ValidationError("Third preference must have a second and first preference before it")
        if pref3 and not pref1:
            raise forms.ValidationError("Third preference must have a second and first preference before it")
        if pref2 and not pref1:
            raise forms.ValidationError("Second preference must have a first preference before it")

        if pref1 == pref2 or pref2 == pref3 or pref1 == pref3:
            raise forms.ValidationError("All courses must be different")