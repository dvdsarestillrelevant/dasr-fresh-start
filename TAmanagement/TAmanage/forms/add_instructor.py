from django import forms
from ..models.instructor_model import Instructor
from .query import Query


class InstructorForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['password'].initial = Instructor.generate_password()

    def clean(self):
        clean_data = super().clean()
        instructor_email = clean_data.get("email")
        if len(Query.filter(email=instructor_email)) >= 1:
            raise forms.ValidationError("User already exists...")
        return clean_data

    class Meta:
        model = Instructor
        fields = ['first_name', 'last_name', 'email', 'password']
