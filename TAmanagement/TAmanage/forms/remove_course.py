from django import forms
from ..models.course_model import Course


class RemoveCourse(forms.Form):
    course = forms.ModelChoiceField(queryset=Course.objects.all(), to_field_name="course_name", required=True)
