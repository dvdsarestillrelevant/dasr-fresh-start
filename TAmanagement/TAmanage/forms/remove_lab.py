from ..models.schedule_model import Schedule
from django import forms
from ..models.schedule_model import Schedule
from ..models.course_model import Course
from ..models.schedule_model import Lab


class RemoveLab(forms.Form):
    lab = forms.ModelChoiceField(queryset=Lab.objects.all(), to_field_name="lab_number", required=True)
