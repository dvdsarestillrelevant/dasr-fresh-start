from django import forms
from ..models.ta_model import TA
from .query import Query


class TAForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['password'].initial = TA.generate_password()

    def clean(self):
        clean_data = super().clean()
        ta_email = clean_data.get("email")
        if len(Query.filter(email=ta_email)) >= 1:
            raise forms.ValidationError("User already exists...")
        return clean_data

    class Meta:
        model = TA
        fields = ['first_name', 'last_name', 'email', 'password']
