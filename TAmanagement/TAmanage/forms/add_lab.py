from django import forms
from ..models.course_model import Course
from datetime import time
import time
import datetime


class LabForm(forms.Form):
    Course = forms.ModelChoiceField(queryset=Course.objects.all(), to_field_name="course_name")
    lab_number = forms.IntegerField(label="Lab Number:")
    choices = [('M', 'Monday'), ('T', 'Tuesday'), ('W', 'Wednesday'), ('R', 'Thursday'), ('F', 'Friday'),
               ('Sa', "Saturday")]
    days = forms.MultipleChoiceField(label="Days:", choices=choices, widget=forms.CheckboxSelectMultiple, required=False)
    start = forms.TimeField(label="Start Time")
    end = forms.TimeField(label="End Time")

    def clean(self):
        cleaned_data = super().clean()
        course = cleaned_data.get("Course")
        number = cleaned_data.get("lab_number")
        days = cleaned_data.get("days")
        start = cleaned_data.get("start")
        end = cleaned_data.get("end")

        if course and number and days and start and end:
            if len(course.labs.filter(lab_number=number)) >= 1:
                raise forms.ValidationError("Lab already exist")
            if start >= end:
                raise forms.ValidationError("End time is before start time")

        cleaned_data.update({"days": "".join(days)})
        collision = False
        for i in range(0, len(days)):
            for j in range(0, len(course.lecture_time.get_days())):
                if days[i] == course.lecture_time.get_days()[j]:
                    collision = True
        # We get all of the days for the lecture
        # First we then check to see if any of the days match. (ie. Lab: MF; Lecture: MR)
        if collision:
            # If they are on the same day then we have to check the time
            # Convert course.get_lecture_time().start() to string if not already to compare
            start_lecture = course.get_lecture_time().get_start().strftime("%H:%M:%S")
            start_lecture = time.strptime(start_lecture.split(',')[0], '%H:%M:%S')
            start_lab = start.strftime("%H:%M:%S")
            start_lab = time.strptime(start_lab.split(',')[0], '%H:%M:%S')

            end_lecture = course.get_lecture_time().get_end().strftime("%H:%M:%S")
            end_lecture = time.strptime(end_lecture.split(',')[0], '%H:%M:%S')
            end_lab = end.strftime("%H:%M:%S")
            end_lab = time.strptime(end_lab.split(',')[0], '%H:%M:%S')

            if start_lab < start_lecture:
                if end_lab < start_lecture:
                    pass
                else:
                    raise forms.ValidationError("There is overlap conflict!")
            elif start_lab > start_lecture:
                if end_lecture < start_lab:
                    pass
                else:
                    raise forms.ValidationError("There is overlap conflict!")
            else:
                # They both start at the same time!
                raise forms.ValidationError("There is overlap conflict!")

            cleaned_data.update({"days": "".join(days)})

        return cleaned_data

#     A(10: 00 - 10:50, MW)
#     B(11: 00 - 11:50, WF)
#
#     loop
#     for days
#         One
#         Day
#         Matches
#         Convert
#         Times
#         to
#         Minutes
#         A - Start = 600
#         A - End = 650
#         B - Start = 660
#         B - End = 710
#     Find Which one Starts First
#     if (A - Start < B - Start)
#         if (A - End < B - Start) Return NO (THIS CASE)
#         if (A - End >= B - Start) Return YES
#     elif (A - Start > B - Start)
#         if (B - End < A - Start) Return NO
#         if (B - End >= A - Start) Return YES
#     else Return YES (Both start at same time)
# No
# Days
# Match: Return
# NO
