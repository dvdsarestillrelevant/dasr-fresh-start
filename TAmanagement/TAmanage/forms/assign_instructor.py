from django import forms
from ..models.instructor_model import Instructor
from ..models.course_model import Course


class InstructorCourse(forms.Form):
    instructor = forms.ModelChoiceField(queryset=Instructor.objects.all(), to_field_name="email")
    course = forms.ModelChoiceField(queryset=Course.objects.all(), to_field_name="course_name")

    # The clean function helps validate that the fields are good.
    # In this case, it validates that the given usernames belong to existing users and that there are no faults.
    def clean(self):
        cleaned_data = super().clean()
        instr = cleaned_data.get("instructor")
        course = cleaned_data.get("course")
        if instr and instr.email and course:
            if len(course.instructor.filter(email=instr.email)) >= 1:
                raise forms.ValidationError("Course already has this Instructor")
        return cleaned_data
