from django import forms
from ..models.instructor_model import Instructor
from ..models.course_model import Course


class UnassignInstructorForm(forms.Form):
    instructor = forms.ModelChoiceField(queryset=Instructor.objects.all(), to_field_name="email")
    course = forms.ModelChoiceField(queryset=Course.objects.all(), to_field_name="course_name")

    def clean(self):
        cleaned_data = super().clean()
        instructor = cleaned_data.get("instructor")
        course = cleaned_data.get("course")

        if len(course.instructor.filter(email=instructor.email)) < 1:
            raise forms.ValidationError("Course does not have this Instructor")

        return cleaned_data
