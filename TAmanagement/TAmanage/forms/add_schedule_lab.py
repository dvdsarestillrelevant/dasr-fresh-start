from ..models.schedule_model import Schedule
from django import forms
from ..models.schedule_model import Schedule
from ..models.lab_model import Lab


class ScheduleLab(forms.Form):
    lab = forms.ModelChoiceField(queryset=Lab.objects.all(), to_field_name="lab_number", required=True)

    def clean(self):
        cleaned_data = super().clean()
        lab = cleaned_data.get("lab")
        if not lab:
            raise forms.ValidationError("You must fill out a lab to submit!")
