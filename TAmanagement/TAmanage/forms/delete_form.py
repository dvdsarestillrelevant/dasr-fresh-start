from django import forms
from ..models.ta_model import TA
from ..models.instructor_model import Instructor
from ..models.course_model import Course
from ..models.lab_model import Lab


class DeleteUser(forms.Form):
    ta = forms.ModelChoiceField(queryset=TA.objects.all(), to_field_name="email", required=False)
    instructor = forms.ModelChoiceField(queryset=Instructor.objects.all(), to_field_name="email", required=False)
    course = forms.ModelChoiceField(queryset=Course.objects.all(), to_field_name="course_name", required=False)
    lab = forms.ModelChoiceField(queryset=Lab.objects.all(), to_field_name="lab_number", required=False)

    # The clean function helps validate that the fields are good.
    # In this case, it validates that the given usernames belong to existing users and that there are no faults.

    def clean(self):
        cleaned_data = super().clean()
        ta = cleaned_data.get("ta")
        instructor = cleaned_data.get("instructor")
        course = cleaned_data.get("course")
        lab = cleaned_data.get("lab")
        if ta is None and instructor is None and course is None and lab is None:
            raise forms.ValidationError("Please fill out at least one field to delete!")
        return cleaned_data
