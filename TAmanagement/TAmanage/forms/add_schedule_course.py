from django import forms
from ..models.course_model import Course


class ScheduleCourse(forms.Form):
    course = forms.ModelChoiceField(queryset=Course.objects.all(), to_field_name="course_name", required=True)

    def clean(self):
        cleaned_data = super().clean()
        course = cleaned_data.get("course")
        if not course:
            raise forms.ValidationError("You must fill out a course to add!")

        return cleaned_data
