from django import forms
from ..models.ta_model import TA
from ..models.course_model import Course


class UnassignTAForm(forms.Form):
    ta = forms.ModelChoiceField(queryset=TA.objects.all(), to_field_name="email")
    course = forms.ModelChoiceField(queryset=Course.objects.all(), to_field_name="course_name")

    def clean(self):
        cleaned_data = super().clean()
        ta = cleaned_data.get("ta")
        course = cleaned_data.get("course")

        if len(course.ta.filter(email=ta.email)) < 1:
            raise forms.ValidationError("Course does not have this TA")

        return cleaned_data
