from django import forms


class ChangePasswordForm(forms.Form):
    new_password1 = forms.CharField(label="New Password", max_length=32)
    new_password2 = forms.CharField(label="New Password Again", max_length=32)

    def clean(self):
        cleaned_data = super().clean()
        temp_new_password1 = cleaned_data.get("new_password1")
        temp_new_password2 = cleaned_data.get("new_password2")

        if temp_new_password1 != temp_new_password2:
            '''new passwords dont match'''
            raise forms.ValidationError("Passwords Do Not Match")

        return cleaned_data
