from django import forms


class BioForm(forms.Form):
    phone_number = forms.CharField(max_length=20)
    address = forms.CharField(max_length=64)

    def clean(self):
        cleaned_data = super().clean()
        phone = cleaned_data.get("phone_number")

        if not validate_phone(phone):
            raise forms.ValidationError("Invalid phone number")


def is_number(stringy):
    for letter in stringy:
        if letter not in '0123456789':
            return False
    return True


def validate_phone(stringy):
    chunks = stringy.replace(' ', '').split('-')
    # 12314567890
    if len(chunks) == 1:
        if len(chunks[0]) != 10:
            return False
        return is_number(chunks[0])

    # (123) 456-7890
    elif len(chunks) == 2:
        last_chunky = chunks[1]
        chunks = chunks[0].strip('(').split(')')
        chunks.append(last_chunky)

        peak = 3.5
        for chunky in chunks:
            if not(is_number(chunky)) or (len(chunky) >= peak or len(chunky) < peak-1):
                return False
            peak += .5
        return True

    # 123-456-7890
    elif len(chunks) == 3:
        peak = 3.5
        for chunky in chunks:
            if not(is_number(chunky)) or (len(chunky) >= peak or len(chunky) < peak-1):
                return False
            peak += .5
        return True
    else:
        return False



