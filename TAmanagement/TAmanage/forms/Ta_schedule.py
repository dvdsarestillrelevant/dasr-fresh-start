from django import forms
from ..models.course_model import Course


class Schedule(forms.Form):
    courses = forms.ModelChoiceField(Course.objects.all())
