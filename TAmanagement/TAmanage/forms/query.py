from ..models.admin_model import Admin
from ..models.ta_model import TA
from ..models.instructor_model import Instructor
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.db.models import QuerySet
from itertools import chain


class Query:
    @staticmethod
    def filter(**kwargs):
        out = []
        out += Admin.objects.filter(**kwargs)
        out += TA.objects.filter(**kwargs)
        out += Instructor.objects.filter(**kwargs)
        return out

    @staticmethod
    def get(**kwargs):
        out = []
        out += Admin.objects.filter(**kwargs)
        out += TA.objects.filter(**kwargs)
        out += Instructor.objects.filter(**kwargs)
        if len(out) == 1:
            return out[0]
        elif len(out) < 1:
            raise ObjectDoesNotExist()
        else:
            raise MultipleObjectsReturned()

    @staticmethod
    def all():
        admin_out = Admin.objects.all()
        ta_out = TA.objects.all()
        instructor_out = Instructor.objects.all()
        return Set(chain(admin_out, ta_out, instructor_out))


class Set:
    def __init__(self, stuff):
        self.stuff = stuff

    def all(self):
        return self.stuff
