from django import forms
from .query import Query


class LoginForm(forms.Form):
    email = forms.CharField(label="Email", max_length=32)
    password = forms.CharField(label="Password", max_length=32, widget=forms.PasswordInput())

    def clean(self):
        cleaned_data = super().clean()
        email = cleaned_data.get("email")
        password = cleaned_data.get("password")
        if email and password:
            if len(Query.filter(email=email, password=password)) != 1:
                raise forms.ValidationError("Invalid Email and Password Combination.")
        return cleaned_data
