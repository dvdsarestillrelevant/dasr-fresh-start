"""TAmanagement URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from TAmanage import views

urlpatterns = [
    path("", views.Home.as_view()),
    path("login", views.Login.as_view()),
    path("logout", views.Logout.as_view()),
    path("form", views.FormView.as_view()),
    path('admin/', admin.site.urls),
    path('labs', views.LabView.as_view()),
    path('ta_info', views.TAView.as_view()),
    path("schedule_course", views.ScheduleView.as_view()),
]
