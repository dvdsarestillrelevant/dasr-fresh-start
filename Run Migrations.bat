@echo off
call venv\Scripts\activate.bat
cd TAmanagement\TAmanage\migrations
del 0001_initial.py
cd ..\..
py manage.py makemigrations
del db.sqlite3
py manage.py sqlmigrate TAmanage 0001
py manage.py migrate
cd ..
py TAmanagement\manage.py loaddata Presets.json
pause
deactivate