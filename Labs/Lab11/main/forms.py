from django.forms import ModelForm
from main import models
 
class AdditemForm(ModelForm):
  class Meta:
    model = models.Additems
    fields = ['name', 'max_score']
    
class StudentForm(ModelForm):
    class Meta:
        model = models.Students
        fields = ['last_name', 'first_name', 'student_ID']

class HomeForm(ModelForm):
  class Meta:
    model = models.Homes
    fields = "__all__"