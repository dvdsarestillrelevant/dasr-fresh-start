from django.conf.urls import url
from django.urls import path
from django.contrib import admin
from main import views

urlpatterns = [
  url(r'^admin/', admin.site.urls),
  path(r'', views.Home.as_view()),
    path(r'home', views.Home.as_view()),
    path(r'student', views.Student.as_view()),
    path(r'additem', views.Additem.as_view()),
  
]
