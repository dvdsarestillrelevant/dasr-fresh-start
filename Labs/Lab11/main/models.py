from django.db import models
from django.core.validators import MinValueValidator

# Create your models here.
class Students(models.Model):
  last_name = models.CharField(max_length=20)
  first_name = models.CharField(max_length=20)
  student_ID = models.IntegerField(validators=[MinValueValidator(1,"Student ID must be postive")], primary_key = True)
  time = models.DateTimeField(auto_now=True)

class Additems(models.Model):
  name = models.CharField(max_length=20, primary_key=True)
  max_score = models.IntegerField(validators=[MinValueValidator(1,"Max score must be postive")])
  time = models.DateTimeField(auto_now=True)

class Homes(models.Model):
  first = models.ForeignKey(Students, related_name="student_id", on_delete=models.CASCADE)
  second = models.ForeignKey(Additems, related_name="class_name",on_delete=models.CASCADE)
  time = models.DateTimeField(auto_now=True)
  score = models.IntegerField(validators=[MinValueValidator(1,"Score must be postive")])