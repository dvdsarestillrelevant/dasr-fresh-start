from django.shortcuts import render
from django.shortcuts import redirect
from django.views import View
from main import models
from main import forms

# Create your views here.
class Home(View):
  def get(self,request):
    homes = list(models.Homes.objects.all().values_list())
    return render(request, 'main/index.html',{"homes":homes,"form":forms.HomeForm()})
  def post(self,request):
    form = forms.HomeForm(request.POST)
    if form.is_valid():
      form.save()
      form = forms.HomeForm()
    homes = list(models.Homes.objects.all().values_list())
    return render(request, 'main/index.html',{"homes":homes})

class Student(View):
  def get(self,request):
    student = list(models.Students.objects.all().values_list('student_ID', flat=True))
    return render(request, 'main/student.html', {"form":forms.StudentForm(),"student":student})
  def post(self,request):
    form = forms.StudentForm(request.POST)
    if form.is_valid(): 
      form.save()      
      form = forms.StudentForm()
    student = list(models.Students.objects.all().values_list('student_ID',flat=True))
    
    return redirect('https://5th-Assignment.winkoski.repl.co')
      
class Additem(View):
  def get(self,request):
    additem = list(models.Additems.objects.all().values_list('name', flat=True))
    return render(request, 'main/additem.html',{"form":forms.AdditemForm(), "additem":additem})
  def post(self,request):
    form = forms.AdditemForm(request.POST)
    if form.is_valid():
      form.save()
      form = forms.AdditemForm()
    additem = list(models.Additems.objects.all().values_list())
    return redirect('https://5th-Assignment.winkoski.repl.co')